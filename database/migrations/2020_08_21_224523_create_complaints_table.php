<?php

use App\Complaint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('code')->unique();
            $table->string('shaki_name');
            $table->string('kode_meli');
            $table->string('shaki_tel');
            $table->string('shaki_address');
            $table->text('matn');
            $table->string('vakil_name');
            $table->string('vakil_address');
            $table->string('vakil_tel');
            $table->enum('status', Complaint::STATUSES_ARRAY)->default(Complaint::CREATED_STATUS);
            $table->boolean('readed')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
}
