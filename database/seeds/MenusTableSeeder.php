<?php

use App\Menu;
use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    public function run()
    {
        Menu::create([
            'title' => 'منو یک',
            'page_id' => 1,
            'order' => 1,
        ]);

        Menu::create([
            'title' => 'منو دو',
            'page_id' => 1,
            'order' => 2,

            'children' => [
                [
                    'title' => 'منو دو - یک',
                    'page_id' => 1,
                    'order' => 1,

                    'children' => [
                        [
                            'title' => 'منو دو - یک - یک',
                            'page_id' => 1,
                            'order' => 1,
                        ],
                        [
                            'title' => 'منو دو - یک - دو',
                            'page_id' => 1,
                            'order' => 2,
                        ],
                    ],
                ],
                [
                    'title' => 'منو دو - دو',
                    'page_id' => 1,
                    'order' => 2,
                ],
            ],
        ]);
    }
}
