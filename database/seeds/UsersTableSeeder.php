<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('palali*')
        ]);
        $admin->assignRole('administrator');

//        $admin = User::create([
//            'name' => 'co_admin',
//            'email' => 'coadmin@coadmin.com',
//            'password' => bcrypt('password')
//        ]);
//
//        $admin->assignRole('admin');
//
//        $user = User::create([
//            'name' => 'user',
//            'email' => 'user@user.com',
//            'password' => bcrypt('password')
//        ]);
//        $user->assignRole('user');

    }
}
