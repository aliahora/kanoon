<?php

use App\Article;
use Illuminate\Database\Seeder;
use Spatie\Tags\Tag;

class ArticleTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Article::find(1)->attachTag(Tag::findOrCreate('برچسب یک'));
        Article::find(1)->attachTag(Tag::findOrCreate('برچسب دو'));
        Article::find(1)->attachTag(Tag::findOrCreate('برچسب سه'));
    }
}
