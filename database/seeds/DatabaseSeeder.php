<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            UsersTableSeeder::class,
            CategoriesTableSeeder::class,
            ArticlesTableSeeder::class,
            ArticleTagsTableSeeder::class,
            PagesTableSeeder::class,
            MenusTableSeeder::class,
            ToolsTableSeeder::class,
            SlidesTableSeeder::class,
            CommentsTableSeeder::class,
            MessagesTableSeeder::class,
            ComplaintsTableSeeder::class,
        ]);
    }
}
