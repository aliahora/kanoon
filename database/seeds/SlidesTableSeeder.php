<?php

use Illuminate\Database\Seeder;

class SlidesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slides')->insert([
            ['article_id' => 1, 'order' => 1],
            ['article_id' => 2, 'order' => 2],
            ['article_id' => 3, 'order' => 3],
        ]);

        DB::table('slides')->insert([
            ['caption' => 'متن مثال اسلاید', 'image' => env('APP_URL') . '/storage/photos/shares/sample-3.jpg', 'link' => 'https://yahoo.com', 'order' => 4],
            ['caption' => 'سال نو مبارک', 'image' => env('APP_URL') . '/storage/photos/shares/sample-4.jpg', 'link' => 'https://microsoft.com', 'order' => 5],
        ]);
    }
}
