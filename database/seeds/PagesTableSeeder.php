<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            ['title' => 'برگه یک', 'slug' => str_slug_persian('برگه یک', '-'), 'published' => true,'body' => '<h1>Heading</h1>'],
            ['title' => 'برگه دو', 'slug' => str_slug_persian('برگه دو', '-'), 'published' => true,'body' => '<h2>Heading</h2>'],
            ['title' => 'برگه سه', 'slug' => str_slug_persian('برگه سه', '-'), 'published' => true,'body' => '<h3>Heading</h3>'],
        ]);
    }
}
