<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $articles = DB::table('articles')->take(3)->get();

        foreach ($articles as $article) {

            for ($i = 1; $i <= 10; $i++) {

                $body = 'نظر برای ' . $article->title . ' ' . $i;

                $id = DB::table('comments')->insertGetId([
                    'article_id' => $article->id,
                    'name' => 'کاربر مهمان',
                    'email' => 'example@gmail.com',
                    'matn' => $body,
                    'published' => true,
                    'readed' => true,
                    'parent_id' => null,
                    'created_at' => now(),
                    'updated_at' => now()
                ]);

                DB::table('comments')->insert([
                    'article_id' => $article->id,
                    'name' => 'کاربر مهمان',
                    'email' => 'example@gmail.com',
                    'matn' => 'پاسخ برای ' . $body,
                    'published' => true,
                    'readed' => true,
                    'parent_id' => $id,
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }
        }
    }
}
