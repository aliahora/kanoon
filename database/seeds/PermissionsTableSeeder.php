<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('cache:clear');
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $operations = [
            'view' => 'مشاهده',
            'store' => 'افزودن',
            'update' => 'بروزرسانی',
            'delete' => 'حذف',
        ];

        $models = [
            'articles' => 'خبر',
            'categories' => 'دسته بندی',
            'comments' => 'نظر',
            'complaints' => 'شکایت',
            'menus' => 'منو',
            'messages' => 'پیام',
            'pages' => 'برگه',
            'permissions' => 'دسترسی',
            'roles' => 'نقش',
            'slides' => 'اسلاید',
            'tools' => 'منو کارتی',
            'users' => 'کاربر',
        ];

        Permission::create([
            'name' => 'users_manage',
            'display_name' => 'مدیریت کاربران',
        ]);


        Permission::create([
            'name' => 'file_manager',
            'display_name' => 'مدیریت فایل ها',
        ]);

        foreach ($models as $modelKey => $model){
            foreach ($operations as $operationKey => $operation){
                Permission::create([
                    'name' => $modelKey . '_' . $operationKey,
                    'display_name' => $operation .' '. $model,
                ]);
            }
        }
    }
}
