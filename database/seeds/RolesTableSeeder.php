<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create([
            'name' => 'administrator',
            'display_name' => 'مدیرکل',

        ]);
        $role->givePermissionTo(Permission::pluck('name'));


        $role = Role::create([
            'name' => 'admin',
            'display_name' => 'مدیر',

        ]);
        $role->givePermissionTo([
            'categories_view',
            'categories_update'
        ]);

        Role::create([
            'name' => 'user',
            'display_name' => 'کاربرعادی',
        ]);
    }
}
