<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ToolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tools')->insert([
            ['title' => 'کارت یک', 'url' => 'https://google.com', 'order' => 1],
            ['title' => 'کارت دو', 'url' => 'https://yahoo.com', 'order' => 2],
            ['title' => 'کارت سه', 'url' => 'https://microsoft.com', 'order' => 3],
        ]);
    }
}
