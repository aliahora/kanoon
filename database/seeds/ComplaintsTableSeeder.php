<?php

use App\Complaint;
use Illuminate\Database\Seeder;

class ComplaintsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('complaints')->insert([
            [
                'code' => generate_complaint_code(),
                'shaki_name' => 'علی اکبرزاده',
                'kode_meli' => '1234567890',
                'shaki_tel' => '09123456789',
                'shaki_address' => 'بوشهر',
                'matn' => 'متن شکایت یک',
                'vakil_name' => 'بیژن اکبرزاده',
                'vakil_tel' => '09193456789',
                'vakil_address' => 'بوشهر، درب سبز',
                'status' => Complaint::CREATED_STATUS,
                'readed' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'code' => generate_complaint_code(),
                'shaki_name' => 'حسین اکبرزاده',
                'kode_meli' => '1234567890',
                'shaki_tel' => '09133456789',
                'shaki_address' => 'بوشهر',
                'matn' => 'متن شکایت دو',
                'vakil_name' => 'اکبر اکبرزاده',
                'vakil_tel' => '09103456789',
                'vakil_address' => 'بوشهر، درب آبی',
                'status' => Complaint::PENDING_STATUS,
                'readed' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'code' => generate_complaint_code(),
                'shaki_name' => 'اکبر اکبرزاده',
                'kode_meli' => '1234567890',
                'shaki_tel' => '09143456789',
                'shaki_address' => 'بوشهر',
                'matn' => 'متن شکایت سه',
                'vakil_name' => 'محمد اکبرزاده',
                'vakil_tel' => '09113456789',
                'vakil_address' => 'بوشهر، درب قرمز',
                'status' => Complaint::PROBLEM_STATUS,
                'readed' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
