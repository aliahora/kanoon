<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert([
            ['title' => 'مطلب یک', 'slug' => str_slug_persian('مطلب یک', '-'), 'category_id' => 1, 'user_id' => 1, 'published' => true, 'body' => 'متن مطلب یک', 'excerpt' => 'چکیده مطلب یک', 'image' => env('APP_URL') . '/storage/photos/shares/sample.jpg', 'created_at' => now(), 'updated_at' => now()],
            ['title' => 'مطلب دو', 'slug' => str_slug_persian('مطلب دو', '-'), 'category_id' => 3, 'user_id' => 1, 'published' => true, 'body' => 'متن مطلب دو', 'excerpt' => 'چکیده مطلب دو', 'image' => env('APP_URL') . '/storage/photos/shares/sample-1.jpg', 'created_at' => now(), 'updated_at' => now()],
            ['title' => 'مطلب سه', 'slug' => str_slug_persian('مطلب سه', '-'), 'category_id' => 2, 'user_id' => 1, 'published' => false, 'body' => 'متن مطلب سه', 'excerpt' => 'چکیده مطلب سه', 'image' => env('APP_URL') . '/storage/photos/shares/sample-2.jpg', 'created_at' => now(), 'updated_at' => now()],
            ['title' => 'مطلب چهار', 'slug' => str_slug_persian('مطلب چهار', '-'), 'category_id' => 2, 'user_id' => 1, 'published' => false, 'body' => 'متن مطلب سه', 'excerpt' => 'چکیده مطلب سه', 'image' => env('APP_URL') . '/storage/photos/shares/sample-2.jpg', 'created_at' => now(), 'updated_at' => now()],
            ['title' => 'مطلب پنج', 'slug' => str_slug_persian('مطلب پنج', '-'), 'category_id' => 2, 'user_id' => 1, 'published' => false, 'body' => 'متن مطلب سه', 'excerpt' => 'چکیده مطلب سه', 'image' => env('APP_URL') . '/storage/photos/shares/sample-2.jpg', 'created_at' => now(), 'updated_at' => now()],
            ['title' => 'مطلب شش', 'slug' => str_slug_persian('مطلب شش', '-'), 'category_id' => 2, 'user_id' => 1, 'published' => false, 'body' => 'متن مطلب سه', 'excerpt' => 'چکیده مطلب سه', 'image' => env('APP_URL') . '/storage/photos/shares/sample-2.jpg', 'created_at' => now(), 'updated_at' => now()],
            ['title' => 'مطلب هفت', 'slug' => str_slug_persian('مطلب هفت', '-'), 'category_id' => 2, 'user_id' => 1, 'published' => false, 'body' => 'متن مطلب سه', 'excerpt' => 'چکیده مطلب سه', 'image' => env('APP_URL') . '/storage/photos/shares/sample-2.jpg', 'created_at' => now(), 'updated_at' => now()],
            ['title' => 'مطلب هشت', 'slug' => str_slug_persian('مطلب هشت', '-'), 'category_id' => 2, 'user_id' => 1, 'published' => false, 'body' => 'متن مطلب سه', 'excerpt' => 'چکیده مطلب سه', 'image' => env('APP_URL') . '/storage/photos/shares/sample-2.jpg', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
