<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['name' => 'دسته بندی یک', 'slug' => str_slug_persian('دسته بندی یک', '-')],
            ['name' => 'دسته بندی دو', 'slug' => str_slug_persian('دسته بندی دو', '-')],
            ['name' => 'دسته بندی سه', 'slug' => str_slug_persian('دسته بندی سه', '-')],
        ]);
    }
}
