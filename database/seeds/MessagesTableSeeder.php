<?php

use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messages')->insert([
            ['name' => 'کاربر مهمان','email' => 'example1@gmail.com', 'subject' => 'موضوع یک', 'readed' => true, 'body' => 'متن پیام یک', 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'کاربر مهمان','email' => 'example2@gmail.com', 'subject' => 'موضوع دو', 'readed' => true, 'body' => 'متن پیام دو', 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'کاربر مهمان','email' => 'example3@gmail.com', 'subject' => 'موضوع سه', 'readed' => true, 'body' => 'متن پیام سه', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
