<?php

namespace Tests\Feature;

use App\Http\Livewire\Site\Login;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    function test_can_login()
    {
        Livewire::test(Login::class)
            ->set('email', 'admin@admin.com')
            ->set('password', 'password')
            ->call('loginUser')->assertRedirect('/admin');
    }
}
