<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use UniSharp\LaravelFilemanager\Lfm;

Route::group(['middleware' => ['auth', 'check_role'], 'prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {
    Route::get('/', 'HomeController@index')->name('dashboard');

    Route::resource('users', 'UserController');
    Route::delete('users_mass_destroy', 'UserController@massDestroy')->name('users.mass_destroy');

    Route::resource('roles', 'RoleController');
    Route::delete('roles_mass_destroy', 'RoleController@massDestroy')->name('roles.mass_destroy');

    Route::resource('permissions', 'PermissionController');
    Route::delete('permissions_mass_destroy', 'PermissionController@massDestroy')->name('permissions.mass_destroy');

    Route::get('articles/search', 'ArticleController@search')->name('articles.search');
    Route::resource('articles', 'ArticleController');

    Route::resource('categories', 'CategoryController');

    Route::resource('pages', 'PageController');

    Route::resource('menus', 'MenuController');

    Route::resource('tools', 'ToolController');

    Route::resource('slides', 'SlideController');

    Route::resource('comments', 'CommentController');

    Route::resource('messages', 'MessageController');

    Route::get('complaints/search', 'ComplaintController@search')->name('complaints.search');
    Route::resource('complaints', 'ComplaintController');

    Route::get('file-manager', 'FileManagerController@index')->name('file-manager');

    Route::group(['prefix' => 'filemanager'], function () {
        Lfm::routes();
    });

    Route::get('attachments/{filename}', function ($filename) {
        $full_path = storage_path('app/attachments/' . $filename);

        if (!File::exists($full_path)) {
            abort(404);
        }

        $file = File::get($full_path);
        $type = File::mimeType($full_path);

        return response($file, 200)->header("Content-Type", $type);
    })->name('attachment');
});

Route::group(['as' => 'site.', 'namespace' => 'Site', 'middleware' => 'visit'], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/complaint', 'ComplaintController@create')->name('complaint');
    Route::post('/complaints', 'ComplaintController@store')->name('complaints.store');

    Route::get('pages/{page}', 'PageController@show')->name('pages.show');

    Route::get('articles/search', 'ArticleController@search')->name('articles.search');
    Route::get('articles/{article}', 'ArticleController@show')->name('articles.show');
    Route::get('articles', 'ArticleController@index')->name('articles.index');
    Route::post('articles/{article}/comments', 'CommentController@store')->name('comments.store');

    Route::get('tags/{tag}', 'TagController@show')->name('tags.show');

    Route::get('categories/{category}', 'CategoryController@show')->name('categories.show');

    Route::post('messages', 'MessageController@store')->name('messages.store');
});

Route::group(['middleware' => 'auth', 'namespace' => 'Auth'], function () {

    Route::post('/logout', 'AuthController@logout')->name('logout');
});


Route::get('test', function () {
    return 'true';
});
