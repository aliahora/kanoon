$(document).ready(function () {
    $(function () {
        var slide_main = $(".slide").slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            asNavFor: ".slide-navigation",
        });
        var slide_sub = $(".slide-navigation").slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 4000,
            speed: 400,
            asNavFor: ".slide",
            focusOnSelect: true,
            vertical: true,
            verticalSwiping: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        centerMode: true,
                        vertical: false,
                        verticalSwiping: false,
                        slidesToShow: 3,
                        adaptiveHeight: true,
                    },
                },
            ],
        });
        var open_window_Width = $(window).width();
        $(window).resize(function () {
            var width = $(window).width();
            if (open_window_Width != width) {
                slide_main.slick("setPosition");
                slide_sub.slick("setPosition");
            }
        });
    });

    $("input[name*='search']").focus(function () {
        $(".mdi-magnify").css("color", "#ff4081").css("transition", "0.2s");
    });

    $("input").focus(function (e) {
        $(e.target)
            .siblings(".mdi")
            .css("color", "#ff4081")
            .css("transition", "0.2s");
    });

    $("input").focusout(function () {
        $(".mdi").css("color", "#000").css("transition", "0.2s");
    });

    $("input").focus(function (e) {
        $(e.target)
            .siblings(".material-icons")
            .css("color", "#ff4081")
            .css("transition", "0.2s");
    });

    $("input").focusout(function (e) {
        $(e.target)
            .siblings(".material-icons")
            .css("color", "#000")
            .css("transition", "0.2s");
    });

    $("#lfm").filemanager("image");

    tinymce.init({
        // path_absolute : "/public",
        selector: "textarea#body",
        language: "fa_IR",
        directionality: "rtl",
        content_css: "/css/material.min.css", // resolved to http://domain.mine/myLayout.css
        height: 400,
        relative_urls: false,
        plugins: "table image",
        file_browser_callback: function (field_name, url, type, win) {
            var x =
                window.innerWidth ||
                document.documentElement.clientWidth ||
                document.getElementsByTagName("body")[0].clientWidth;
            var y =
                window.innerHeight ||
                document.documentElement.clientHeight ||
                document.getElementsByTagName("body")[0].clientHeight;

            var cmsURL = "/admin/filemanager?field_name=" + field_name;
            if (type == "image") {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file: cmsURL,
                title: "مدیریت فایل",
                width: x * 0.8,
                height: y * 0.8,
                resizable: "yes",
                close_previous: "no",
            });
        },
    });

    $("#tags").tagsInput({
        // min/max number of characters
        minChars: 3,
        maxChars: 20,

        // max number of tags
        // limit: 6,

        // RegExp
        validationPattern: null,
        // TODO: set pattern

        // duplicate validation
        unique: true,
        placeholder: "برچسب را وارد کنید + اینتر",
    });

    $('[data-tooltip="tooltip"]').tooltip();






    $(".dropdown-menu > .dropdown > a").addClass("dropdown-toggle");

    $(".dropdown-menu a.dropdown-toggle").on("click", function (e) {
        if (!$(this).next().hasClass("show")) {
            $(this)
                .parents(".dropdown-menu")
                .first()
                .find(".show")
                .removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass("show");
        $(this)
            .parents("li.nav-item.dropdown.show")
            .on("hidden.bs.dropdown", function (e) {
                $(".dropdown-menu > .dropdown .show").removeClass("show");
            });
        return false;
    });
});
