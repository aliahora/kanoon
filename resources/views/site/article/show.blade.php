@extends('site.partials.master')
@section('site.title', $article->title)

@section('main')

    <div class="container" id="article-show">
        <div class="row">
            <div class="col-md-8 card mx-auto my-4">
                @include('layouts.flash-message')
                <div class="image mt-2 mx-auto">
                    <img src="{!! $article->image !!}" alt="image" height="400px">
                </div>


                <h3 class="my-4 article_title">
                    {!! $article->title !!}
                </h3>
                <div class="article_body">
                    <p>
                        {!! $article->matn !!}
                    </p>

                </div>

                <div class="text-black-50 mb-3 d-flex justify-content-between">
                    <div>
                        <span class="material-icons">schedule</span>
                        <span class="time">{{ verta($article->created_at)->format('j F Y') ?? '' }}</span>

                        <span>|</span>

                        <span class="material-icons">insert_chart_outlined</span>
                        <span>بازدید: </span>
                        <span class="stats">{{ $article->visitLogs()->count() }}</span>
                    </div>

                    <div>
                        نویسنده: {{$article->user->name}}
                    </div>

                </div>

                <div class="mb-3">
                    <h4> <span class="material-icons">local_offer</span>برچسب ها</h4>
                    @foreach($article->tags as $tag)
                        <span class="badge badge-info p-1"><a href="#"></a>{{ $tag->name }}</span>
                    @endforeach
                </div>

                <h4 class="mt-5"><span class="material-icons">insert_comment</span>نظرات</h4>


                @foreach($article->comments as $comment)
                    @if(!$comment->parent_id)

                        <div class="media">
                            <img src="{{asset('img/profile.png')}}" alt="" width="71" height="auto">
                            <div class="media-body card article_comment mb-2 pr-2">
                                <div class="d-flex justify-content-between align-items-center">
                                    <h6 class="article_comment_name pt-1 ">{{$comment->name}}</h6>
                                    <p class="ml-1 mt-1">{{ verta($comment->created_at)->format('j F Y') ?? '' }}</p>
                                </div>

                                <span class="article_comment_body">{{$comment->matn}}</span>
                                {{--<a href="#" class="reply mb-1 ml-2 align-self-end">پاسخ به نظر</a>--}}
                            </div>
                        </div>
                    @endif
                    {{--@if ( $comment->replies )--}}
                        {{--@foreach($comment->replies as $rep1)--}}
                            {{--<div class="media mr-4 my-2">--}}
                                {{--<img src="{{asset('img/profile.png')}}" alt="" width="71" height="auto">--}}
                                {{--<div class="media-body card article_comment_child pr-2">--}}
                                    {{--<div class="d-flex justify-content-between">--}}
                                        {{--<h6 class="pt-2">{{ $rep1->name }}</h6>--}}
                                        {{--<p class="ml-1 mt-1">{{ verta($comment->created_at)->format('j F Y') ?? '' }}</p>--}}
                                    {{--</div>--}}
                                    {{--<span class="py-2">{{ $rep1->matn }}</span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endforeach--}}
                    {{--@endif--}}
                @endforeach

                <div class="mt-5"><span class="material-icons">mode_comment </span>نظرات خود را با ما به اشتراک بگذارید</div>


                <form action="{{ route('site.comments.store',$article)}}" method="POST">
                    @csrf
                    {{--<input type="hidden" name="article_id" value="{{$article->id}}">--}}
                    <div class="row">
                        <div class="col-sm-6 mt-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="mdi mdi-format-title"> </span>
                                    <input type="text" name="name" value="{{ old('name') }}"
                                           class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           id="name" placeholder="نام...">
                                    <div class="invalid-feedback">
                                        @error('name')
                                        {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="mr-1" for="matn">متن نظر</label>
                        <div class="input-group">
                            <span class="mdi mdi-format-name"> </span>
                            <textarea name="matn" id="matn"
                                      class="form-control mt-2 {{ $errors->has('matn') ? ' is-invalid' : '' }}">{{ old('matn') }}</textarea>
                            <div class="invalid-feedback">
                                @error('matn')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="form-group mt-4 w-50">
                        <label class="mr-1" for="captcha">کد امنیتی</label>
                        <div class="input-group">
                            <input name="captcha" id="captcha"
                                   class="form-control {{ $errors->has('captcha') ? ' is-invalid' : '' }}">
                            <img src="{{ captcha_src('flat') }}" alt="">
                            <div class="invalid-feedback">
                                @error('captcha')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>
                    </div>

                    <button class="btn btn-success mb-2" type="submit">
                            <span class="material-icons">
                                save
                            </span> ارسال
                    </button>

                </form>

            </div>
            <div class="col-md-4">

                <div class="card mt-4 mr-4">
                    <div class="card-header">
                        موضوعات
                    </div>
                    <ul class="list-group list-group-flush">
                        @foreach ($categories as $category)
                            <a href="{{ route('site.categories.show', $category) }}">
                                <li class="list-group-item">{{ $category->name }}</li>
                            </a>
                        @endforeach
                    </ul>
                </div>


                <div class="card mt-3 mr-4">
                    <div class="card-header mr-3 mt-2">
                        لو هولتز
                    </div>
                    <div class="card-body">
                        اگر از زندگی خسته شده‌اید، اگر هر روز با یك آرزوی داغ برای انجام كاری از خواب بیدار نمی‌شوید،
                        برای این است كه به اندازه‌ی كافی هدف ندارید.
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
