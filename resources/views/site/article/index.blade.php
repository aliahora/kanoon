@extends('site.partials.master')
@section('site.title', 'اخبار')

@section('main')

    <div class="container" id="articles-index">
        <div class="row my-3">
            <div class="col-md-8">
                @foreach ($articles as $article)
                    <article class="py-3">
                        <div class="card">
                            <a href="{{ route('site.articles.show', $article) }}"><img class="card-img-top w-100"
                                    height="300px" src="{{ $article->image }}" alt="{{ $article->title }}"></a>
                            <div class="card-body">
                                <h5 class="card-title mb-4">{{ $article->title }}</h5>
                                <p class="card-subtitle mb-4 text-muted">{{ $article->category->name }}</p>
                                <p class="card-text">{{ $article->excerpt }}</p>
                                <div class="d-flex justify-content-between align-items-center">


                                    <div class="text-black-50">
                                        <span class="material-icons">schedule</span>
                                        <span class="time">{{ verta($article->created_at)->format('j F Y') ?? '' }}</span>

                                        <span>|</span>

                                        <span class="material-icons">insert_chart_outlined</span>
                                        <span>بازدید: </span>
                                        <span class="stats">{{ $article->visitLogs()->count() }}</span>

                                    </div>
                                    <a href="{{ route('site.articles.show', $article) }}" class="btn btn-sm btn-primary">
                                        <span class="material-icons">
                                            launch
                                        </span>
                                        <span>ادامه مطلب</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </article>
                @endforeach
            </div>
            <div class="col-md-4 pt-3">
                <div class="card">
                    <div class="card-header">
                        موضوعات
                    </div>
                    <ul class="list-group list-group-flush">
                        @foreach ($categories as $category)
                            <a href="{{ route('site.categories.show', $category) }}">
                                <li class="list-group-item">{{ $category->name }}</li>
                            </a>
                        @endforeach
                    </ul>
                </div>

                <div class="card mt-3">
                    <div class="card-title mr-3 mt-2">
                        لو هولتز
                    </div>
                    <div class="card-body">
                        اگر از زندگی خسته شده‌اید، اگر هر روز با یك آرزوی داغ برای انجام كاری از خواب بیدار نمی‌شوید، برای این است كه به اندازه‌ی كافی هدف ندارید.
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
