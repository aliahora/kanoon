@extends('site.partials.master')
@section('site.title', $page->title)

@section('main')

    {!! $page->body !!}

@endsection
