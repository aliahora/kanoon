<div class="parallax tools">

    <div class="row">

        <div class="col-md-9">
            <div class="row">
                <div class="text-center col-6 col-lg-3">
                    <a href="{{route('site.complaint')}}">
                        <div class="card">

                            <div class="card-body justify-content-center d-flex align-items-center">

                                <h5 class="card-title">ثبت شکایات</h5>

                            </div>

                        </div>
                    </a>
                </div>

                <div class="text-center col-6 col-lg-3">

                    <a href="#" data-toggle="modal" data-target="#complaintShow">
                        <div class="card">

                            <div class="card-body justify-content-center d-flex align-items-center">

                                <h5 class="card-title">پیگیری شکایات</h5>

                            </div>

                        </div>
                    </a>

                </div>
                @foreach($tools as $tool)
                    <div class="text-center col-6 col-lg-3">
                        <a href="{{$tool->url}}">
                            <div class="card">

                                <div class="card-body justify-content-center d-flex align-items-center">

                                    <h5 class="card-title ">{{$tool->title}}</h5>

                                </div>

                            </div>
                        </a>
                    </div>
                @endforeach

            </div>
        </div>

        <div class="text-center col-md-3">
            <div class="row">
                <div class="col-sm-6 col-md-12">
                    <div class=" card">
                        <div class="card-body">

                            <i class="mdi mdi-gavel"></i>
                            <span>۷۰۰+</span>
                            <h5 class="card-title ">کمیسیون ها</h5>

                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-md-12">
                    <div class=" card">
                        <div class="card-body">

                            <i class="mdi mdi-gavel"></i>
                            <span>۷۰۰+</span>
                            <h5 class="card-title ">کمیسیون ها</h5>

                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-md-12">
                    <div class=" card">
                        <div class="card-body">

                            <i class="mdi mdi-gavel"></i>
                            <span>۷۰۰+</span>
                            <h5 class="card-title ">کمیسیون ها</h5>

                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-md-12">
                    <div class=" card">
                        <div class="card-body">

                            <i class="mdi mdi-gavel"></i>
                            <span>۷۰۰+</span>
                            <h5 class="card-title ">کمیسیون ها</h5>

                        </div>

                    </div>
                </div>

            </div>


        </div>


        <div id="complaintShow" class="modal fade">
            <div class="modal-dialog" role="document">
                @livewire('site.complaint-search')
            </div>
        </div>

    </div>
</div>
