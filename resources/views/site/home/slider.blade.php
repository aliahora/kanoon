<div class="slide-box">
    <ul class="slide">
        @foreach ($slides as $slide)

            @if ($slide->article_id)
                <li class="item">
                    <a href="{{ route('site.articles.show', $slide->article) }}"><img src="{{ $slide->article->image }}"
                            alt="{{ $slide->article->title }}" />

                        <h5>{{ $slide->article->title }}</h5>
                    </a>

                </li>
            @else
                <li class="item">
                    <a href="{{ $slide->link }}" target="_blank"><img src="{{ $slide->image }}"
                            alt="{{ $slide->caption }}" />

                        <h5>{{ $slide->caption }}</h5>
                    </a>

                </li>
            @endif

        @endforeach
    </ul>
    <div class="slide-navigation">
        @foreach ($slides as $slide)

            @if ($slide->article_id)
                <div class="item">
                    <figure class="image">
                        <img src="{{ $slide->article->image }}" />
                    </figure>
                </div>
            @else
                <div class="item">
                    <figure class="image">
                        <img src="{{ $slide->image }}" />
                    </figure>
                </div>
            @endif

        @endforeach
    </div>
</div>
