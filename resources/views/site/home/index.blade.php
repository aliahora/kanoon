@extends('site.partials.master')
@section('site.title', 'صفحه اصلی')

@section('main')
    @include('site.home.slider')
    @include('site.home.articles')
    @include('site.home.tools')

@endsection
