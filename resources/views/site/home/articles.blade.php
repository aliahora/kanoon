<section class="px-5 py-4 mx-auto">
    <h3 class="mt-3">آخرین اخبار</h3>
    <div class="row articles w-100">
        @foreach ($articles as $article)
            <article class="col-md-3 col-sm-6 py-3">
                <div class="card">
                    <a href="{{ route('site.articles.show', $article) }}"><img class="card-img-top" height="200px"
                            src="{{ $article->image }}" alt="{{ $article->title }}"></a>
                    <div class="card-body">
                        <h5 class="card-title">{{ $article->title }}</h5>
                        <p class="card-text">{{ $article->excerpt }}</p>
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="{{ route('site.articles.show', $article) }}" class="btn btn-sm btn-primary">ادامه
                                مطلب</a>
                            <div class="text-black-50">
                                <span class="time">{{ verta($article->created_at)->format('j F Y') ?? '' }}</span>
                                <span class="material-icons">schedule</span>
                            </div>
                        </div>

                    </div>
                </div>
            </article>
        @endforeach
    </div>
</section>
