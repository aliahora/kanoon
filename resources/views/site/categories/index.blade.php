@extends('site.partials.master')
@section('site.title', 'اخبار')

@section('main')

    <div class="container" id="articles-index">
        <div class="row my-3">
            <div class="col-md-8">
                @foreach ($catarticles as $catarticle)
                    <article class="py-3">
                        <div class="card">
                            <a href="{{ route('site.articles.show', $catarticle) }}"><img class="card-img-top w-100"
                                                                                       height="300px" src="{{ $catarticle->image }}" alt="{{ $catarticle->title }}"></a>
                            <div class="card-body">
                                <h5 class="card-title mb-4">{{ $catarticle->title }}</h5>
                                <p class="card-text">{{ $catarticle->excerpt }}</p>
                                <div class="d-flex justify-content-between align-items-center">


                                    <div class="text-black-50">
                                        <span class="material-icons">schedule</span>
                                        <span class="time">{{ verta($catarticle->created_at)->format('j F Y') ?? '' }}</span>

                                        <span>|</span>

                                        <span class="material-icons">insert_chart_outlined</span>
                                        <span>بازدید: </span>
                                        <span class="stats">{{ $catarticle->visitLogs()->count() }}</span>

                                    </div>
                                    <a href="{{ route('site.articles.show', $catarticle) }}" class="btn btn-sm btn-primary">
                                        <span class="material-icons">
                                            launch
                                        </span>
                                        <span>ادامه مطلب</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </article>
                @endforeach
            </div>
            <div class="col-md-4 pt-3">

                <div class="card mt-3">
                    <div class="card-title mr-3 mt-2">
                        لو هولتز
                    </div>
                    <div class="card-body">
                        اگر از زندگی خسته شده‌اید، اگر هر روز با یك آرزوی داغ برای انجام كاری از خواب بیدار نمی‌شوید، برای این است كه به اندازه‌ی كافی هدف ندارید.
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
