@extends('site.partials.master')
@section('site.title', 'ثبت شکایت')

@section('main')

    <div class="row container-fluid my-4 w-100">
        <div class="offset-1"></div>
        <div class="col-sm-3">
            <h3>ثبت شکایت</h3>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi consequuntur culpa distinctio in ipsum iure
                iusto nostrum nulla obcaecati praesentium quidem rem repellat reprehenderit, temporibus totam veniam, vero
                voluptas voluptatum!
            </p>
        </div>
        <div class="col-sm-7">
            <div id="complaint" class="card">

                <div class="card-body">
                    @include('layouts.flash-message')
                    <form action="{{ route('site.complaints.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="card-title">مشخصات موکل</div>
                                <div class="form-group">
                                    <label for="shaki_name">نام و نام خانوادگی</label>
                                    <div class="input-group">
                                        <span class="mdi mdi-format-title"> </span>
                                        <input type="text" name="shaki_name" value="{{ old('shaki_name') }}"
                                            class="form-control {{ $errors->has('shaki_name') ? ' is-invalid' : '' }}"
                                            id="shaki_name" placeholder="فارسی وارد شود" required>
                                        <div class="invalid-feedback">
                                            @error('shaki_name')
                                            {{ $message }}
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kode_meli">کد ملی</label>
                                    <div class="input-group">
                                        <span class="material-icons">
                                            credit_card
                                        </span>
                                        <input type="number" name="kode_meli"
                                        maxlength="10"
                                        placeholder="10 رقم"
                                        value="{{ old('kode_meli') }}"
                                            class="form-control {{ $errors->has('kode_meli') ? ' is-invalid' : '' }}"
                                            id="kode_meli" minlength="10" required>
                                        <div class="invalid-feedback">
                                            @error('kode_meli')
                                            {{ $message }}
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="shaki_tel">شماره تلفن</label>
                                    <div class="input-group">
                                        <span class="material-icons">
                                            local_phone
                                        </span>
                                        <input type="number" name="shaki_tel"
                                        value="{{ old('shaki_tel') }}"
                                            class="form-control {{ $errors->has('shaki_tel') ? ' is-invalid' : '' }}"
                                            id="shaki_tel" placeholder="09123456789" required>
                                        <div class="invalid-feedback">
                                            @error('shaki_tel')
                                            {{ $message }}
                                            @enderror
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="shaki_address">آدرس</label>
                                    <div class="input-group">
                                        <span class="material-icons">
                                            business
                                        </span>
                                        <input type="text" name="shaki_address"
                                        value="{{ old('shaki_address') }}"
                                        placeholder="بوشهر، ..."
                                            class="form-control {{ $errors->has('shaki_address') ? ' is-invalid' : '' }}"
                                            id="shaki_address" required>
                                        <div class="invalid-feedback">
                                            @error('shaki_address')
                                            {{ $message }}
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="card-title">مشخصات وکیل</div>
                                <div class="form-group">
                                    <label for="vakil_name">نام و نام خانوادگی </label>
                                    <div class="input-group">
                                        <span class="mdi mdi-format-title"> </span>
                                        <input type="text" name="vakil_name"
                                        value="{{ old('vakil_name') }}"
                                            class="form-control {{ $errors->has('vakil_name') ? ' is-invalid' : '' }}"
                                            id="vakil_name" placeholder="فارسی وارد شود" required>
                                        <div class="invalid-feedback">
                                            @error('vakil_name')
                                            {{ $message }}
                                            @enderror
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="vakil_tel">شماره تلفن</label>
                                    <div class="input-group">
                                        <span class="material-icons">
                                            local_phone
                                        </span>
                                        <input type="number" name="vakil_tel"
                                        value="{{ old('vakil_tel') }}"
                                        placeholder="09123456789"
                                            class="form-control {{ $errors->has('vakil_tel') ? ' is-invalid' : '' }}"
                                            id="vakil_tel" required>
                                        <div class="invalid-feedback">
                                            @error('vakil_tel')
                                            {{ $message }}
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="vakil_address">آدرس</label>
                                    <div class="input-group">
                                        <span class="material-icons">
                                            business
                                        </span>
                                        <input type="text" name="vakil_address"
                                        value="{{ old('vakil_address') }}"
                                        placeholder="بوشهر، ..."
                                            class="form-control {{ $errors->has('vakil_address') ? ' is-invalid' : '' }}"
                                            id="vakil_address" required>
                                        <div class="invalid-feedback">
                                            @error('vakil_address')
                                            {{ $message }}
                                            @enderror
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="form-group mt-4">
                            <label class="mr-1" for="attachments">انتخاب فایل <span class="text-black">( حداکثر هر فایل 700 کیلوبایت | پسوند های مجاز: jpg, jpeg, png, pdf, doc, docx)</span></label>
                            <div class="input-group">
                                <span id="attachment-icon" class="material-icons">attachment</span>
                                <input name="attachments[]" multiple type="file"
                                value="{{ old('attachments') }}"
                                    class="form-control-file {{ $errors->has('attachments') ? ' is-invalid' : '' }}">
                                <div class="invalid-feedback">
                                    @error('attachments')
                                    {{ $message }}
                                    @enderror
                                </div>
                            </div>
                        </div>


                        <div class="form-group mt-4">
                            <label class="mr-1" for="matn">متن شکایت</label>
                            <div class="input-group">
                                <span class="mdi mdi-format-name"> </span>
                                <textarea name="matn" id="matn" class="form-control mt-2 {{ $errors->has('matn') ? ' is-invalid' : '' }}">
                                    {{ old('matn') }}
                                </textarea>
                                <div class="invalid-feedback">
                                    @error('matn')
                                    {{ $message }}
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-4 w-50">
                            <label class="mr-1" for="captcha">کد امنیتی</label>
                            <div class="input-group">
                                <input name="captcha" id="captcha"
                                    class="form-control {{ $errors->has('captcha') ? ' is-invalid' : '' }}">
                                <img src="{{ captcha_src('flat') }}" alt="">
                                <div class="invalid-feedback">
                                    @error('captcha')
                                    {{ $message }}
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-success" type="submit">
                            <span class="material-icons">
                                save
                            </span> ارسال
                        </button>

                    </form>


                </div>
            </div>
        </div>
    </div>



@endsection
