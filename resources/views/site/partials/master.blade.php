@extends('layouts.app')
@section('title')
    @yield('site.title') - کانون وکلا استان بوشهر

@endsection



    @section('content')
        <div class="content">
        @include('site.partials.header')
        @include('site.partials.navbar')
        {{--<div class="container-fluid">--}}

        @yield('main')
        {{--</div>--}}
        </div>
        @include('site.partials.footer')
    @endsection







