<div class="d-md-flex pt-3 px-4 align-items-center">
    <div class="flex-grow-1 text-center text-md-right">
        <span>{{ \Hekmatinasser\Verta\Verta::now()->format('l j F Y') . ' | ' . \Carbon\Carbon::now()->format('l j F Y') }}</span>
    </div>


    <div class="login mt-2 mt-md-0">
        @guest
            <button type="button" class="btn btn-flat-info btn-sm " data-toggle="modal" data-target="#login-modal"
                    data-whatever="@mdo">
                <span class="material-icons ml-1">login</span>
                {{--<img src="https://img.icons8.com/material/21/law--v1.svg"/>--}}
                ورود

            </button>
            <button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#register-modal">
                <span class="material-icons ml-1">person_add</span>
                عضویت
            </button>

        @else



            @unlessrole('user')
                <a href="{{route('admin.dashboard')}}" class="btn btn-flat-info btn-sm">پنل مدیریت </a>
            @endunlessrole

            <div class="btn btn-info btn-sm dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    {{auth()->user()->name}}
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('site.home')}}"> <span
                                class="material-icons ml-1">person</span>پروفایل</a>
                    <form class="" id="logout" method="post" action="{{route('logout')}}">
                        @csrf
                        <button type="submit" class="dropdown-item">
                            <span class="material-icons">power_settings_new</span>
                            خروج
                        </button>
                    </form>
                </div>
            </div>


        @endguest

    </div>

    <div class="search">
        <form action="{{ route('site.articles.search') }}" method="get">
            <div class="has-search fg--search">
                <button type="submit"><span class="mdi mdi-magnify form-control-feedback"></span></button>
                <input name="q" value="{{ request()->input('q') ?? '' }}" type="text" class="form-control" placeholder="جستجو">
            </div>

        </form>

    </div>


</div>


<div id="banner" class="mt-3 mt-md-0">
    <div class="d-flex justify-content-between align-items-end">
        <div class="logo"><a href="{{ route('site.home') }}"><img src="{{asset('img/kanoon.png')}}"></a></div>
        <div class="d-none d-sm-block mainBanner"><img src="{{asset('img/headerBackground.png')}}"></div>
    </div>

</div>

<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        @livewire('site.login')
    </div>
</div>

<div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        @livewire('site.register')
    </div>
</div>

