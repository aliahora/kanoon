<div class="footer">
    <div class="row ">
        <div class="col-md-3">
            <h5>آمار کاربران</h5>
            <div>امروز :{{ $statistics['today'] }}</div>
            <div>دیروز :{{ $statistics['yesterday'] }}</div>
            <div>هفته جاری :{{ $statistics['this_week'] }}</div>
            <div>هفته گذشته :{{ $statistics['last_week'] }}</div>
            <div>ماه جاری :{{ $statistics['this_month'] }}</div>
            <div>ماه گذشته :{{ $statistics['last_month'] }}</div>
            <div>بازدید کل :{{ $statistics['total'] }}</div>
        </div>
        <div class="col-md-3">
            <h5>پیوندهای مرتبط</h5>
            <div><a href="http://www.hamivakil.ir/" target="_blank">صندوق حمایت وکلا و کارگشایان دادگستری</a></div>
            <div><a href="http://www.dadiran.ir/" target="_blank">قوه قضائیه</a></div>
            <div><a href="http://icbar.ir/" target="_blank">کانون وکلای دادگستری مرکز</a></div>
            <div><a href="http://www.scoda.ir/" target="_blank">اتحادیه سراسری کانون های وکلای دادگستری ایران</a></div>
            <div><a href="http://www.rrk.ir/Default.aspx" target="_blank">روزنامه رسمی</a></div>
            <div><a href="http://www.dadsetani.ir/" target="_blank">دادستانی کل کشور</a></div>
        </div>
        <div class="col-md-3">
            <h5>نظرسنجی</h5>
        </div>
        <div class="col-md-3">
            <h5>تماس با ما</h5>
            <table>
                <tr>
                    <th>
                        <span class="material-icons">location_on</span>
                    </th>
                    <td>
                        آدرس : بوشهر، خیابان بهشت صادق، رو به روی خبرگزاری جمهوری اسلامی، کانون وکلای دادگستری استان
                        بوشهر

                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="material-icons">local_post_office</span>
                    </th>
                    <td>
                        کد پستی: 59774-75149

                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="material-icons">phone</span>
                    </th>
                    <td>
                        تلفن تماس: 3541929

                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="material-icons">print</span>
                    </th>
                    <td>
                        فکس: 3541930

                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="copy-right d-block d-sm-flex justify-content-between text-center">
        <div>تمام حقوق این سایت متعلق به کانون وکلای استان بوشهر میباشد.</div>
        <div><a href="#"> طراحی و اجرا: حمیدرضا اکبرزاده </a></div>
    </div>

</div>


