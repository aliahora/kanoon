<nav class="navbar navbar-expand-lg bg-primary text-white navbar-light">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">

            <li class="nav-item">
                <a class="nav-link" href="{{ route('site.home') }}">صفحه اصلی <span class="sr-only">(current)</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('site.articles.index') }}">اخبار <span class="sr-only">(current)</span></a>
            </li>

            @foreach ($menus as $menu)

                @if (count($menu->children))

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                            {{ $menu->title }}
                        </a>
                        <ul class="dropdown-menu">
                            @foreach ($menu->children as $child)

                                @if (count($child->children))

                                    <li class="dropdown">
                                        <a class="dropdown-item" href="#">{{ $child->title }}</a>
                                        <ul class="dropdown-menu">

                                            @foreach ($child->children as $child2)
                                                <li><a class="dropdown-item" href="{{ route('site.pages.show', $child2->page) }}">{{ $child2->title }}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>

                                @else

                                    <li>
                                        <a class="dropdown-item"
                                            href="{{ route('site.pages.show', $child->page) }}">{{ $child->title }}
                                        </a>
                                    </li>

                                @endif
                            @endforeach

                        </ul>
                    </li>
                @else

                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('site.pages.show', $menu->page) }}">{{ $menu->title }}</a>
                    </li>

                @endif

            @endforeach
        </ul>
    </div>
</nav>
