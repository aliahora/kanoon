<div class="mr-4">
    @foreach ($childrens as $child)
        <div class="card my-3 mr-4">
            <div class="d-flex justify-content-between card-body">
                <span>{{ $child->title }}</span>
                <div>
                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                        data-target="#edit-modal{{ $child->id }}">
                        <span class="material-icons">
                            create
                        </span> ویرایش
                    </button>
                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                        data-target="#delete-modal{{ $child->id }}">
                        <span class="material-icons">
                            delete
                        </span> حذف
                    </button>
                </div>
            </div>
        </div>
        @if (count($child->children))
            @include('admin.menus.manage-children',['childrens' => $child->children])
        @endif


        <div class="modal fade" id="edit-modal{{ $child->id }}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <form action="{{ route('admin.menus.update', $child->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">ویرایش منو</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="title">عنوان</label>
                                <input name="title" type="text" value="{{ $child->title }}" class="form-control"
                                    id="title" required>
                            </div>
                            <div class="form-group">
                                <label for="parent">والد</label>
                                <select name="parent_id" class="form-control" id="parent">
                                    <option></option>
                                    @foreach ($allMenus as $m)
                                        <option value="{{ $m->id }}"
                                            {{ $child->parent_id == $m->id ? 'selected' : '' }}>
                                            {{ $m->title }}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="page">برو به</label>
                                <select name="page_id" class="form-control" id="page" required>
                                    @foreach ($pages as $page)
                                        <option value="{{ $page->id }}"
                                            {{ $child->page_id == $page->id ? 'selected' : '' }}>{{ $page->title }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="order">ترتیب</label>
                                <input name="order" type="number" value="{{ $child->order }}" class="form-control"
                                    id="order" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" data-dismiss="modal" type="button">انصراف</button>
                            <input class="btn text-white bg-info" type="submit" value="ذخیره">
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div id="delete-modal{{ $child->id }}" class="modal fade">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <p class="text-black typography-subheading">آیا مطمئنید؟</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-info" data-dismiss="modal" type="button">خیر</button>
                        <form action="{{ route('admin.menus.destroy', $child->id) }}" method="POST">
                            @method('DELETE') @csrf
                            <input class="btn text-white bg-info" type="submit" value="بلی">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
