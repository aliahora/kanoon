@extends('admin.partials.master')
@section('admin.title','ساخت منو')
@section('main')
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-create-menu">
        <span class="material-icons">
            add
        </span> افزودن منو
    </button>
    <div class="mt-3">
        <div class="card-header">
            منوها
        </div>

        <div class="card-body">


            @foreach ($menus as $menu)
                    <div class="card my-3">
                        <div class="d-flex justify-content-between card-body">
                            <span>{{ $menu->title }}</span>
                            <div>
                                <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                                        data-target="#edit-modal{{ $menu->id }}">
                                <span class="material-icons">
                                    create
                                </span> ویرایش
                                </button>
                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                        data-target="#delete-modal{{ $menu->id }}">
                                <span class="material-icons">
                                    delete
                                </span> حذف
                                </button>
                            </div>
                        </div>
                    </div>
                @if (count($menu->children))
                    @include('admin.menus.manage-children',['childrens' => $menu->children])
                @endif

                <div class="modal fade" id="edit-modal{{ $menu->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <form action="{{ route('admin.menus.update', $menu->id) }}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">ویرایش منو</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="title">عنوان</label>
                                        <input name="title" type="text" value="{{ $menu->title }}" class="form-control"
                                            id="title" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="parent">والد</label>
                                        <select name="parent_id" class="form-control" id="parent">
                                            <option></option>
                                            @foreach ($allMenus as $m)
                                                <option value="{{ $m->id }}"
                                                    {{ $menu->parent_id == $m->id ? 'selected' : '' }}>{{ $m->title }}
                                                </option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="page">برو به</label>
                                        <select name="page_id" class="form-control" id="page" required>
                                            @foreach ($pages as $page)
                                                <option value="{{ $page->id }}"
                                                    {{ $menu->page_id == $page->id ? 'selected' : '' }}>{{ $page->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="order">ترتیب</label>
                                        <input name="order" type="number" value="{{ $menu->order }}" class="form-control"
                                            id="order" required>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-info" data-dismiss="modal" type="button">انصراف</button>
                                    <input class="btn text-white bg-info" type="submit" value="ذخیره">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


                <div id="delete-modal{{ $menu->id }}" class="modal fade">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <p class="text-black typography-subheading">آیا مطمئنید؟</p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-info" data-dismiss="modal" type="button">خیر</button>
                                <form action="{{ route('admin.menus.destroy', $menu->id) }}" method="POST">
                                    @method('DELETE') @csrf
                                    <input class="btn text-white bg-info" type="submit" value="بلی">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
    </div>

    <div class="modal fade" id="modal-create-menu" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{ route('admin.menus.store') }}" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">ایجاد منو</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="title">عنوان</label>
                            <input name="title" type="text" class="form-control" id="title" required>
                        </div>
                        <div class="form-group">
                            <label for="parent">والد</label>
                            <select name="parent_id" class="form-control" id="parent">
                                <option></option>
                                @foreach ($allMenus as $m)
                                    <option value="{{ $m->id }}">{{ $m->title }}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="page">برو به</label>
                            <select name="page_id" class="form-control" id="page" required>
                                @foreach ($pages as $page)
                                    <option value="{{ $page->id }}">{{ $page->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="order">ترتیب</label>
                            <input name="order" type="number" class="form-control" id="order" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-info" data-dismiss="modal" type="button">انصراف</button>
                        <input class="btn text-white bg-info" type="submit" value="ذخیره">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
