@extends('admin.partials.master')
@section('admin.title',' مشاهده نظر')
@section('main')

    <div class="card w-50-lg">
        <div class="card-header">
            مشاهده  نظر
        </div>

        <div class="card-body">
            <div class="mb-2">
                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <th>
                                نام
                            </th>
                            <td>
                                {{ $comment->name }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                ایمیل
                            </th>
                            <td>
                                {{ $comment->email ?? 'بدون ایمیل' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                متن
                            </th>
                            <td>
                                {{ $comment->matn }}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <a style="margin-top:20px;" class="btn btn-info" href="{{ url()->previous() }}">
                    <span class="material-icons">
                        toc
                        </span>         برگشت به لیست
                </a>
            </div>
        </div>
    </div>
@endsection
