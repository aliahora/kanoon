@extends('admin.partials.master')
@section('admin.title', 'نظرات')
@section('main')
    <div class="card mt-3">
        <div class="card-header">
            نظرات
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>
                                متن
                            </th>
                            <th>
                                خبر
                            </th>
                            <th>
                                وضعیت
                            </th>
                            <th>
                                تاریخ ایجاد
                            </th>
                            <th>
                                عملیات
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($comments as $key => $comment)
                            <tr>
                                <td>
                                <div class="text-truncate {{ $comment->readed ? '' : 'font-weight-bold' }}" style="width: 200px;">
                                        {{ $comment->matn ?? '' }}
                                    </div>
                                </td>
                                <td>
                                    <a
                                        href="{{ route('admin.articles.show', $comment->article) }}">{{ $comment->article->title }}</a>
                                </td>
                                <td>
                                    @if ($comment->published)
                                        <span class="badge badge-success badge-pill py-1 px-2"> تایید شده </span>
                                    @else
                                        <span class="badge badge-warning text-white badge-pill py-1 px-2"> تایید نشده </span>
                                    @endif
                                </td>
                                <td>
                                    {{ verta($comment->created_at)->format('j F Y | H:i') ?? '' }}
                                </td>
                                <td>
                                    <form class="d-inline" action="{{ route('admin.comments.update', $comment) }}"
                                        method="POST">
                                        @method('PUT') @csrf

                                        @if ($comment->published)
                                            <button class="btn btn-float btn-warning text-white btn-sm" type="submit"
                                                data-tooltip="tooltip" data-placement="bottom" title="عدم تایید">
                                                <span class="material-icons">close</span>
                                            </button>
                                        @else
                                            <button class="btn btn-float btn-success btn-sm" type="submit"
                                                data-tooltip="tooltip" data-placement="bottom" title="تایید">
                                                <span class="material-icons">check</span>
                                            </button>
                                        @endif


                                    </form>

                                    <a class="btn btn-float btn-sm btn-success"
                                        href="{{ route('admin.comments.show', $comment) }}" data-tooltip="tooltip"
                                        data-placement="bottom" title="مشاهده">
                                        <span class="material-icons">
                                            remove_red_eye
                                        </span>
                                    </a>

                                    <a class="btn btn-float btn-sm btn-info"
                                        href="{{ route('admin.comments.edit', $comment) }}" data-tooltip="tooltip"
                                        data-placement="bottom" title="ویرایش">
                                        <span class="material-icons">
                                            create
                                        </span>
                                    </a>

                                    <button type="button" class="btn btn-float btn-danger btn-sm" data-toggle="modal"
                                        data-target="#delete-modal{{ $comment->id }}" data-tooltip="tooltip"
                                        data-placement="bottom" title="حذف">
                                        <span class="material-icons">
                                            delete
                                        </span>
                                    </button>
                                </td>
                            </tr>
                            <div id="delete-modal{{ $comment->id }}" class="modal fade">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <p class="text-black typography-subheading">آیا مطمئنید؟</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-info" data-dismiss="modal" type="button">خیر</button>
                                            <form action="{{ route('admin.comments.destroy', $comment->id) }}"
                                                method="POST">
                                                @method('DELETE') @csrf
                                                <input class="btn text-white bg-info" type="submit" value="بلی">
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
                {{ $comments->onEachSide(1)->links() }}
            </div>


        </div>
    </div>
@endsection
