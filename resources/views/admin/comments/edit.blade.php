@extends('admin.partials.master')
@section('admin.title', 'ویرایش نظر')
@section('main')

    <div class="card w-50-lg">
        <div class="card-header">
            ویرایش نظر
        </div>

        <div class="card-body">
            <form action="{{ route('admin.comments.update', $comment) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">نام</label>
                    <div class="input-group">
                        <span class="mdi mdi-format-name"> </span>
                        <input type="text" name="name" value="{{ $comment->name }}"
                            class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name">
                        <div class="invalid-feedback">
                            @error('name')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">ایمیل</label>
                    <div class="input-group">
                        <input type="text" name="email" value="{{ $comment->email }}"
                            class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email">
                        <div class="invalid-feedback">
                            @error('email')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="matn">متن</label>
                    <div class="input-group">
                        <textarea style="min-height: 300px;" class="form-control {{ $errors->has('matn') ? ' is-invalid' : '' }}" id="matn"
                            name="matn" required>{{ $comment->matn }}</textarea>
                        <div class="invalid-feedback">
                            @error('matn')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div>
                    <button class="btn btn-success" type="submit">
                        <span class="material-icons">
                            save
                        </span> بروزرسانی
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
