@extends('admin.partials.master')
@section('admin.title', 'داشبورد')
@section('main')
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="card text-white bg-success mb-3">
                    <div class="card-body">
                        <h5 class="card-title">
                            <span class="material-icons">person</span>
                            <span>عنوان</span>
                        </h5>
                        <p class="card-text">متن مثال</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card text-white bg-warning mb-3">
                    <div class="card-body">
                        <h5 class="card-title">
                            <span class="material-icons">person</span>
                            <span>عنوان</span>
                        </h5>
                        <p class="card-text">متن مثال</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card text-white bg-info mb-3">
                    <div class="card-body">
                        <h5 class="card-title">
                            <span class="material-icons">person</span>
                            <span>عنوان</span>
                        </h5>
                        <p class="card-text">متن مثال</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card text-white bg-danger mb-3">
                    <div class="card-body">
                        <h5 class="card-title">
                            <span class="material-icons">person</span>
                            <span>عنوان</span>
                        </h5>
                        <p class="card-text">متن مثال</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header bg-success text-white d-flex justify-content-between align-items-center">
                <span>نظرات امروز</span>
                <a class="btn btn-sm btn-flat-light" href="{{ route('admin.comments.index') }}">مشاهده همه</a>
            </div>
            <div class="card-body">
                @if (count($comments))
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>
                                    متن
                                </th>
                                <th>
                                    خبر
                                </th>
                                <th>
                                    وضعیت
                                </th>
                                <th>
                                    تاریخ ایجاد
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($comments as $key => $comment)
                                <tr>
                                    <td>
                                    <div class="text-truncate {{ $comment->readed ? '' : 'font-weight-bold' }}" style="width: 200px;">
                                            {{ $comment->body ?? '' }}
                                        </div>
                                    </td>
                                    <td>
                                        <a
                                            href="{{ route('admin.articles.show', $comment->article) }}">{{ $comment->article->title }}</a>
                                    </td>
                                    <td>
                                        @if ($comment->published)
                                            <span class="badge badge-success badge-pill py-1 px-2"> تایید شده </span>
                                        @else
                                            <span class="badge badge-warning text-white badge-pill py-1 px-2"> تایید نشده </span>
                                        @endif
                                    </td>
                                    <td>
                                        {{ verta($comment->created_at)->format('j F Y | H:i') ?? '' }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div>امروز نظری ثبت نشده است.</div>
                @endif
            </div>
        </div>
        <div class="card mt-3">
            <div class="card-header bg-warning text-white d-flex justify-content-between align-items-center">
                <span>پیام های امروز</span>
                <a class="btn btn-sm btn-flat-light" href="{{ route('admin.messages.index') }}">مشاهده همه</a>
            </div>
            <div class="card-body">
                @if (count($messages))
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        متن
                                    </th>
                                    <th>
                                        تاریخ ایجاد
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($messages as $key => $message)
                                    <tr>
                                        <td>
                                            <div class="text-truncate {{ $message->readed ? '' : 'font-weight-bold' }}"
                                                style="width: 600px;">
                                                {{ $message->body ?? '' }}
                                            </div>
                                        </td>
                                        <td>
                                            {{ verta($message->created_at)->format('j F Y | H:i') ?? '' }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div>امروز پیامی ثبت نشده است.</div>
                @endif
            </div>
        </div>
        <div class="card mt-3">
            <div class="card-header bg-info text-white d-flex justify-content-between align-items-center">
                <span>شکایات امروز</span>
                <a class="btn btn-sm btn-flat-light" href="{{ route('admin.complaints.index') }}">مشاهده همه</a>
            </div>
            <div class="card-body">
                @if (count($complaints))
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        کد پیگیری
                                    </th>
                                    <th>
                                        نام شاکی
                                    </th>
                                    <th>
                                        تلفن شاکی
                                    </th>
                                    <th>
                                        وضعیت
                                    </th>
                                    <th>
                                        تاریخ ایجاد
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($complaints as $key => $complaint)
                                    <tr>
                                        <td>
                                            <div class="text-truncate {{ $complaint->readed ? '' : 'font-weight-bold' }}">
                                                {{ $complaint->code ?? '' }}
                                            </div>
                                        </td>
                                        <td>
                                            {{ $complaint->shaki_name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $complaint->shaki_tel ?? '' }}
                                        </td>
                                        <td>
                                            @switch($complaint->status)
                                                @case(App\Complaint::CREATED_STATUS)
                                                <span class="badge badge-info badge-pill py-1 px-2"> ثبت شده </span>
                                                @break
                                                @case(App\Complaint::PENDING_STATUS)
                                                <span class="badge badge-warning text-white badge-pill py-1 px-2"> در حال پیگیری
                                                </span>
                                                @break
                                                @case(App\Complaint::DENIED_STATUS)
                                                <span class="badge badge-danger badge-pill py-1 px-2"> رد شده </span>
                                                @break
                                                @case(App\Complaint::ACCEPTED_STATUS)
                                                <span class="badge badge-success badge-pill py-1 px-2"> پذیرفته شده </span>
                                                @break
                                                @case(App\Complaint::PROBLEM_STATUS)
                                                <span class="badge badge-danger badge-pill py-1 px-2"> اطلاعات ناقص </span>
                                                @break
                                                @case(App\Complaint::FINISHED_STATUS)
                                                <span class="badge badge-success badge-pill py-1 px-2"> خاتمه یافته </span>
                                                @break
                                            @endswitch
                                        </td>
                                        <td>
                                            {{ verta($complaint->created_at)->format('j F Y | H:i') ?? '' }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div>امروز شکایتی ثبت نشده است.</div>
                @endif
            </div>
        </div>
    </div>
@endsection
