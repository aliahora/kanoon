@php
use Illuminate\Support\Str;

function checkRoute($name) {
if(Route::current() && Str::contains(Route::current()->getName(),$name))
echo'active';
}
@endphp


<div aria-hidden="true" class="navdrawer navdrawer-right navdrawer-permanent-lg" id="navdrawerDefault" tabindex="-1">
    <div class="navdrawer-content">
        <div class="navdrawer-header">
            <a class="navbar-brand px-0" href="{{ route('admin.dashboard') }}">پنل مدیریت</a>
        </div>
        <nav class="navdrawer-nav">
            <a target="_blank" class="nav-item nav-link" href="{{ route('site.home') }}"><i
                    class="material-icons mr-3">home</i>
                صفحه اصلی</a>
        </nav>
        <div class="navdrawer-divider"></div>
        <nav class="navdrawer-nav">
            <a class="nav-item nav-link {{ checkRoute('categories.') }}" href="{{ route('admin.categories.index') }}"><i
                    class="material-icons mr-3">dialpad</i>
                دسته بندی ها</a>
            <a class="nav-item nav-link {{ checkRoute('articles.') }}" href="{{ route('admin.articles.index') }}"><i
                    class="material-icons mr-3">article</i>
                اخبار</a>
            <a class="nav-item nav-link {{ checkRoute('comments.') }}" href="{{ route('admin.comments.index') }}"><i
                    class="material-icons mr-3">comment</i>
                <span>نظرات</span>
                @if ($countOfNewComments > 0)
                    <span class="badge badge-info badge-pill float-left">{{ $countOfNewComments }}</span>
                @endif

            </a>
            <a class="nav-item nav-link {{ checkRoute('slides.') }}" href="{{ route('admin.slides.index') }}"><i
                    class="material-icons mr-3">view_carousel</i>
                اسلاید ها</a>
            <a class="nav-item nav-link {{ checkRoute('users.') }}" href="{{ route('admin.users.index') }}"><i
                    class="material-icons mr-3">groups</i> کاربران</a>
            <a class="nav-item nav-link {{ checkRoute('messages.') }}" href="{{ route('admin.messages.index') }}"><i
                    class="material-icons mr-3">forum</i>
                <span>پیام ها</span>
                @if ($countOfNewMessages > 0)
                    <span class="badge badge-info badge-pill float-left">{{ $countOfNewMessages }}</span>
                @endif
            </a>
            <a class="nav-item nav-link {{ checkRoute('complaints.') }}" href="{{ route('admin.complaints.index') }}"><i
                    class="material-icons mr-3">assignment_late</i>
                <span>شکایات</span>
                @if ($countOfNewComplaints > 0)
                    <span class="badge badge-info badge-pill float-left">{{ $countOfNewComplaints }}</span>
                @endif
            </a>

        </nav>
        <div class="navdrawer-divider"></div>
        <div class="navdrawer-subheader">پیشرفته</div>
        <nav class="navdrawer-nav">
            <a class="nav-item nav-link {{ checkRoute('permissions.') }}"
                href="{{ route('admin.permissions.index') }}"><i class="material-icons mr-3">rule</i>
                دسترسی ها</a>
            <a class="nav-item nav-link {{ checkRoute('roles.') }}" href="{{ route('admin.roles.index') }}"><i
                    class="material-icons mr-3">admin_panel_settings</i>
                نقش ها</a>
            <a class="nav-item nav-link {{ checkRoute('menus.') }}" href="{{ route('admin.menus.index') }}"><i
                    class="material-icons mr-3">menu</i>
                ساخت منو</a>
            <a class="nav-item nav-link {{ checkRoute('pages.') }}" href="{{ route('admin.pages.index') }}"><i
                    class="material-icons mr-3">menu_book</i>
                برگه ها</a>
            <a class="nav-item nav-link {{ checkRoute('tools.') }}" href="{{ route('admin.tools.index') }}"><i
                    class="material-icons mr-3">view_module</i>
                منوی کارتی</a>
            <a class="nav-item nav-link {{ checkRoute('file-manager') }}" href="{{ route('admin.file-manager') }}"><i
                    class="material-icons mr-3">folder</i>
                مدیریت فایل</a>
        </nav>
    </div>
</div>
