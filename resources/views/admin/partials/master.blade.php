@extends('layouts.app')
@section('title')
    پنل مدیریت - @yield('admin.title')
@endsection
@section('content')

    @include('admin.partials.header')
    @include('admin.partials.sidebar')

    <div class="mt-5 py-4 px-3 doc-navbar-permanent">
        @include('layouts.flash-message')
        @yield('main')
    </div>

@endsection
