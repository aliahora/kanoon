<nav class="admin-nav fixed-top navbar navbar-dark navbar-full bg-primary doc-navbar-permanent">
    <button aria-controls="navdrawerDefault" data-breakpoint="lg" data-type="permanent" aria-expanded="false"
        aria-label="Toggle Navdrawer" class="navbar-toggler d-lg-none" data-target="#navdrawerDefault"
        data-toggle="navdrawer"><span class="navbar-toggler-icon"></span></button>
    <ul class="navbar-nav justify-content-end">
        <li class="nav-item align-self-center flex-grow-1">کانون وکلا استان بوشهر</li>
        <li class="nav-item dropdown">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                {{ auth()->user()->name }}
            </a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="{{ route('site.home') }}"> <span
                        class="material-icons ml-1">person</span>پروفایل</a>
                <form class="" id="logout" method="post" action="{{ route('logout') }}">
                    @csrf
                    <button type="submit" class="dropdown-item">
                        <span class="material-icons">power_settings_new</span>
                        خروج
                    </button>
                </form>
            </div>
        </li>

    </ul>
</nav>
