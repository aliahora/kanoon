@extends('admin.partials.master')
@section('admin.title', 'ویرایش منو کارتی')
@section('main')

    <div class="card w-50-lg">
        <div class="card-header">
            ویرایش {{ $tool->title }}
        </div>

        <div class="card-body">
            <form action="{{ route('admin.tools.update', [$tool->id]) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="title">عنوان</label>
                    <div class="input-group">
                        <span class="mdi mdi-format-title"> </span>
                        <input type="text" name="title" value="{{ $tool->title }}"
                            class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" id="title"
                            placeholder="فارسی وارد شود" required>
                        <div class="invalid-feedback">
                            @error('title')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="url">آدرس</label>
                    <div class="input-group">
                        <span class="mdi mdi-format-url"> </span>
                        <input type="url" name="url" value="{{ $tool->url }}"
                            class="form-control {{ $errors->has('url') ? ' is-invalid' : '' }}" id="url"
                            placeholder="مثال https://google.com" required>
                        <div class="invalid-feedback">
                            @error('url')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="order">ترتیب</label>
                    <div class="input-group">
                        <span class="mdi mdi-format-url"> </span>
                        <input type="number" name="order" value="{{ $tool->order }}"
                               class="form-control {{ $errors->has('order') ? ' is-invalid' : '' }}" id="order"
                               placeholder="مثال: 1 یا 2 یا 3 ..." required>
                        <div class="invalid-feedback">
                            @error('order')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div>
                    <button class="btn btn-success" type="submit">
                        <span class="material-icons">
                            save
                        </span> بروزرسانی
                    </button>
                </div>
            </form>


        </div>
    </div>
@endsection
