@extends('admin.partials.master')
@section('admin.title', 'منو های کارتی')
@section('main')
    <a class="btn btn-success" href="{{ route('admin.tools.create') }}">
        <span class="material-icons">
            add
        </span> افزودن منو کارتی
    </a>
    <div class="card mt-3">
        <div class="card-header">
            منو های کارتی
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>
                                عنوان
                            </th>
                            <th>
                                آدرس
                            </th>
                            <th>
                                ترتیب
                            </th>
                            <th>
                                عملیات
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tools as $key => $tool)
                            <tr>
                                <td>
                                    {{ $tool->title ?? '' }}
                                </td>
                                <td>
                                    {{ $tool->url ?? '' }}
                                </td>
                                <td>
                                    {{ $tool->order ?? '' }}
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-info" href="{{ route('admin.tools.edit', $tool->id) }}">
                                        <span class="material-icons">
                                            create
                                        </span> ویرایش
                                    </a>

                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                        data-target="#delete-modal{{ $tool->id }}">
                                        <span class="material-icons">
                                            delete
                                        </span> حذف
                                    </button>
                                </td>
                            </tr>
                            <div id="delete-modal{{ $tool->id }}" class="modal fade">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <p class="text-black typography-subheading">آیا مطمئنید؟</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-info" data-dismiss="modal" type="button">خیر</button>
                                            <form action="{{ route('admin.tools.destroy', $tool->id) }}" method="POST">
                                                @method('DELETE') @csrf
                                                <input class="btn text-white bg-info" type="submit" value="بلی">
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
