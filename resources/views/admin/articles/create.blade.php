@extends('admin.partials.master')
@section('admin.title', 'ایجاد خبر')
@section('main')

    <div>
        <div class="card-header">
            ایجاد خبر
        </div>

        <div class="card-body">
            <form action="{{ route('admin.articles.store') }}" method="POST">@csrf
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="title">عنوان</label>
                                    <div class="input-group">
                                        <span class="mdi mdi-format-title"> </span>
                                        <input type="text" name="title" value="{{ old('title') }}"
                                            class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" id="title"
                                            placeholder="فارسی وارد شود" required>
                                        <div class="invalid-feedback">
                                            @error('title')
                                            {{ $message }}
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="mb-2" for="body"> محتوا </label>
                                    <textarea name="body" id="body"
                                        class="form-control {{ $errors->has('body') ? ' is-invalid' : '' }}">{{ old('body') }}</textarea>
                                    <div class="invalid-feedback">
                                        @error('body')
                                        {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                                <input type="hidden" value="{{ auth()->id() }}" name="user_id" required>
                                <div class="form-group">
                                    <label class="mb-2" for="excerpt"> چکیده </label>
                                    <textarea id="excerpt" name="excerpt"
                                        class="form-control {{ $errors->has('excerpt') ? ' is-invalid' : '' }}"
                                        required>{{ old('excerpt') }}</textarea>
                                    <div class="invalid-feedback">
                                        @error('excerpt')
                                        {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-3">
                            <button class="btn btn-success" type="submit">
                                <span class="material-icons">
                                    save
                                </span> ذخیره
                            </button>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header bg-info text-white">
                                جزئیات
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="mb-1" for="category_id"> دسته بندی </label>
                                    <select name="category_id" id="category_id"
                                        class="form-control {{ $errors->has('category_id') ? ' is-invalid' : '' }}"
                                        required>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}"
                                                {{ old('category_id') == $category->id ? 'selected' : '' }}>
                                                {{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        @error('category_id')
                                        {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="mb-1" for="published"> وضعیت </label>
                                    <select name="published" id="published"
                                        class="form-control {{ $errors->has('published') ? ' is-invalid' : '' }}" required>
                                        <option value="1" {{ old('published') == 1 ? 'selected' : '' }}>منتشر شده</option>
                                        <option value="0" {{ old('published') == 0 ? 'selected' : '' }}>پیشنویس</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        @error('published')
                                        {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                                <div class="custom-control custom-checkbox mt-4">
                                    <input name="featured" value="1" type="checkbox" class="custom-control-input"
                                        id="customCheck1" {{ old('featured') == 1 ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="customCheck1">خبر ویژه</label>
                                </div>


                                <div class="form-group mt-4">
                                    <label for="tags">برچسب ها</label>
                                    <div class="input-group">
                                        <input type="text" name="tags" value="{{ old('tags') }}"
                                               class="form-control {{ $errors->has('tags') ? ' is-invalid' : '' }}" id="tags"
                                               placeholder="فارسی وارد شود">
                                        <div class="invalid-feedback">
                                            @error('tags')
                                            {{ $message }}
                                            @enderror
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                        <div class="card mt-3">
                            <div class="card-header bg-info text-white">
                                تصویر
                            </div>

                            <div class="card-body">

                                    <span class="input-group-btn">
                                        <a id="lfm" data-input="thumbnail" data-preview="holder"
                                            class="btn btn-info text-white">
                                            <span class="material-icons">insert_photo</span> انتخاب
                                        </a>
                                    </span>
                                    <input placeholder="تصویر را انتخاب کنید" id="thumbnail" class="form-control"
                                        value="{{ old('image') }}" type="text" name="image" readonly required>

                            </div>
                        </div>

                        <div class="card mt-3">
                            <div class="card-header bg-success text-white">
                                محتوا سئو
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="seo_title">عنوان سئو</label>
                                    <div class="input-group">
                                        <span class="mdi mdi-format-title"> </span>
                                        <input type="text" name="seo_title" value="{{ old('seo_title') }}"
                                            class="form-control {{ $errors->has('seo_title') ? ' is-invalid' : '' }}"
                                            id="seo_title" placeholder="فارسی وارد شود">
                                        <div class="invalid-feedback">
                                            @error('seo_title')
                                            {{ $message }}
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="mb-1" for="meta_description"> توضیحات متا </label>
                                    <textarea id="meta_description" name="meta_description"
                                        class="form-control {{ $errors->has('meta_description') ? ' is-invalid' : '' }}">{{ old('meta_description') }}</textarea>
                                    <div class="invalid-feedback">
                                        @error('meta_description')
                                        {{ $message }}
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="mb-1" for="meta_keywords"> کلیدواژه های متا </label>
                                    <textarea id="meta_keywords" placeholder="با , جدا شود" name="meta_keywords"
                                        class="form-control {{ $errors->has('meta_keywords') ? ' is-invalid' : '' }}">{{ old('meta_keywords') }}</textarea>
                                    <div class="invalid-feedback">
                                        @error('meta_keywords')
                                        {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
