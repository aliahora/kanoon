@extends('admin.partials.master')
@section('admin.title','مشاهده خبر')
@section('main')

    <div class="card">
        <div class="card-header">
            مشاهده {{ $article->title  }}
        </div>

        <div class="card-body">
            <div class="mb-2">
                <h4>تصویر</h4>
                <img src="{{ $article->image  }}" class="w-50">
                <br>
                <br>
                <div>
                    @if($article->published)
                        <span class="badge badge-success badge-pill py-1 px-2"> منتشر شده </span>
                    @else
                        <span class="badge badge-warning text-white badge-pill py-1 px-2"> پیشنویس </span>
                    @endif
                        @if($article->featured)
                            <span class="badge badge-danger badge-pill py-1 px-2"> خبر ویژه </span>
                        @endif
                </div>
                <br>
                <br>
                {!! $article->body !!}
                <br>
                <br>
                <h4>نویسنده</h4>
                {{$article->user->name}}
                <br>
                <br>
                <h4>دسته بندی</h4>
                {{$article->category->name}}
                <br>
                <br>
                <h4>چکیده</h4>
                {{$article->excerpt}}
                <br>
                <br>
                <h4>برچسب ها</h4>
                {{$article->tagsToString()}}

                <br>
                <br>

                <h4>عنوان سئو</h4>
                {{$article->seo_title}}
                <br>
                <br>
                <h4>کلیدواژه های متا</h4>
                {{$article->meta_keywords}}
                <br>
                <br>

                <h4>توضیحات متا</h4>
                {{$article->meta_description}}
                <br>

                <h4> ادرس صفحه -> <a href="{{ route('site.articles.show', $article)}}">لینک</a></h4>


                <br>
                <a style="margin-top:20px;" class="btn btn-info" href="{{ url()->previous() }}">
                <span class="material-icons">
                    toc
                    </span> برگشت به لیست
                </a>
            </div>
        </div>
    </div>
@endsection
