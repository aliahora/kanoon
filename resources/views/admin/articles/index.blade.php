@extends('admin.partials.master')
@section('admin.title', 'خبر ها')
@section('main')
    <a class="btn btn-success" href="{{ route('admin.articles.create') }}">
        <span class="material-icons">
            add
        </span> افزودن خبر
    </a>
    <div class="card mt-3">
        <div class="card-header d-flex justify-content-between">
            <div class="align-self-center">خبر ها</div>
            <div class="search w-25">
                <form action="{{ route('admin.articles.search') }}" method="GET">
                    <div class="has-search fg--search">
                        <button type="submit"><span class="mdi mdi-magnify form-control-feedback"></span></button>
                    <input name="q" value="{{ request()->input('q') ?? '' }}" type="text" class="form-control" placeholder="جستجو">
                    </div>

                </form>

            </div>

        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>
                                تصویر
                            </th>
                            <th>
                                عنوان
                            </th>
                            <th>
                                وضعیت
                            </th>

                            <th>
                                تاریخ ایجاد
                            </th>
                            <th>
                                عملیات
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($articles as $key => $article)
                            <tr>
                                <td>
                                    <img width="60px" src="{{ thumb($article->image) }}" alt="عکس">
                                </td>
                                <td>
                                    {{ $article->title ?? '' }}
                                </td>
                                <td>
                                    @if ($article->published)
                                        <span class="badge badge-success badge-pill py-1 px-2"> منتشر شده </span>
                                    @else
                                        <span class="badge badge-warning text-white badge-pill py-1 px-2"> پیشنویس </span>
                                    @endif
                                </td>
                                <td>
                                    {{ verta($article->created_at)->format('j F Y | H:i') ?? '' }}
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-success"
                                        href="{{ route('admin.articles.show', $article->slug) }}">
                                        <span class="material-icons">
                                            remove_red_eye
                                        </span> مشاهده
                                    </a>

                                    <a class="btn btn-sm btn-info"
                                        href="{{ route('admin.articles.edit', $article->slug) }}">
                                        <span class="material-icons">
                                            create
                                        </span> ویرایش
                                    </a>

                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                        data-target="#delete-modal{{ $article->id }}">
                                        <span class="material-icons">
                                            delete
                                        </span> حذف
                                    </button>
                                </td>
                            </tr>
                            <div id="delete-modal{{ $article->id }}" class="modal fade">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <p class="text-black typography-subheading">آیا مطمئنید؟</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-info" data-dismiss="modal" type="button">خیر</button>
                                            <form action="{{ route('admin.articles.destroy', $article->slug) }}"
                                                method="POST">
                                                @method('DELETE') @csrf
                                                <input class="btn text-white bg-info" type="submit" value="بلی">
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
                {{ $articles->onEachSide(1)->links() }}
            </div>


        </div>
    </div>
@endsection
