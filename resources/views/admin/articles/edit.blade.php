@extends('admin.partials.master')
@section('admin.title', 'ویرایش خبر')
@section('main')

    <div>
        <div class="card-header">
            ویرایش {{ $article->title }}
        </div>

        <div class="card-body">
            <form action="{{ route('admin.articles.update', $article) }}" method="POST">@csrf @method('PUT')
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="title">عنوان</label>
                                    <div class="input-group">
                                        <span class="mdi mdi-format-title"> </span>
                                        <input type="text" name="title" value="{{ $article->title }}"
                                               class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}"
                                               id="title"
                                               placeholder="فارسی وارد شود" required>
                                        <div class="invalid-feedback">
                                            @error('title')
                                            {{ $message }}
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="mb-2" for="body"> محتوا </label>
                                    <textarea name="body" id="body"
                                              class="form-control {{ $errors->has('body') ? ' is-invalid' : '' }}">{{ $article->body }}</textarea>
                                    <div class="invalid-feedback">
                                        @error('body')
                                        {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="mb-2" for="excerpt"> چکیده </label>
                                    <textarea id="excerpt" name="excerpt"
                                              class="form-control {{ $errors->has('excerpt') ? ' is-invalid' : '' }}"
                                              required>{{ $article->excerpt }}</textarea>
                                    <div class="invalid-feedback">
                                        @error('excerpt')
                                        {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-3">
                            <button class="btn btn-success" type="submit">
                                <span class="material-icons">
                                    save
                                </span> بروزرسانی
                            </button>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header bg-info text-white">
                                جزئیات
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="mb-1" for="category_id"> دسته بندی </label>
                                    <select name="category_id" id="category_id"
                                            class="form-control {{ $errors->has('category_id') ? ' is-invalid' : '' }}"
                                            required>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}"
                                                {{ $article->category_id == $category->id ? 'selected' : '' }}>
                                                {{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        @error('category_id')
                                        {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="mb-1" for="published"> وضعیت </label>
                                    <select name="published" id="published"
                                            class="form-control {{ $errors->has('published') ? ' is-invalid' : '' }}"
                                            required>
                                        <option value="1" {{ $article->published ? 'selected' : '' }}>منتشر شده</option>
                                        <option value="0" {{ !$article->published ? 'selected' : '' }}>پیشنویس</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        @error('published')
                                        {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                                <div class="custom-control custom-checkbox mt-4">
                                    <input name="featured" value="1" type="checkbox" class="custom-control-input"
                                           id="customCheck1"
                                        {{ $article->featured ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="customCheck1">خبر ویژه</label>
                                </div>

                                <div class="form-group mt-4">
                                    <label for="tags">برچسب ها</label>
                                    <div>
                                        <input class="form-control {{ $errors->has('tags') ? ' is-invalid' : '' }}"
                                               id="tags" name="tags" type="text"
                                               value="{{ $article->tagsToString() }}"/>
                                        <div class="invalid-feedback">
                                            @error('tags')
                                            {{ $message }}
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="card mt-3">
                            <div class="card-header bg-info text-white">
                                تصویر
                            </div>
                            <div class="card-body">
                                <img class="w-100" src="{{ $article->image }}">
                                <div class="input-group mt-3">
                                    <span class="input-group-btn">
                                        <a id="lfm" data-input="thumbnail" data-preview="holder"
                                           class="btn btn-info text-white">
                                            <span class="material-icons">insert_photo</span> انتخاب
                                        </a>
                                    </span>
                                    <input placeholder="تصویر را انتخاب کنید" id="thumbnail"
                                           value="{{ $article->image  }}" class="form-control" type="text" name="image"
                                           required readonly>
                                </div>
                            </div>
                        </div>
                        <div class="card mt-3">
                            <div class="card-header bg-success text-white">
                                محتوا سئو
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="seo_title">عنوان سئو</label>
                                    <div class="input-group">
                                        <span class="mdi mdi-format-title"> </span>
                                        <input type="text" name="seo_title" value="{{ $article->seo_title }}"
                                               class="form-control {{ $errors->has('seo_title') ? ' is-invalid' : '' }}"
                                               id="seo_title" placeholder="فارسی وارد شود">
                                        <div class="invalid-feedback">
                                            @error('seo_title')
                                            {{ $message }}
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="mb-1" for="meta_description"> توضیحات متا </label>
                                    <textarea id="meta_description" name="meta_description"
                                              class="form-control {{ $errors->has('meta_description') ? ' is-invalid' : '' }}">
                                        {{ $article->meta_description }}</textarea>
                                    <div class="invalid-feedback">
                                        @error('meta_description')
                                        {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="mb-1" for="meta_keywords"> کلیدواژه های متا </label>
                                    <textarea id="meta_keywords" placeholder="با , جدا شود" name="meta_keywords"
                                              class="form-control {{ $errors->has('meta_keywords') ? ' is-invalid' : '' }}">{{ $article->meta_keywords }}</textarea>
                                    <div class="invalid-feedback">
                                        @error('meta_keywords')
                                        {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
