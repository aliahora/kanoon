@extends('admin.partials.master') @section('admin.title', 'نقش ها')
@section('main')

    <a class="btn btn-success" href="{{ route('admin.roles.create') }}">
        <span class="material-icons"> add </span> افزودن نقش
    </a>
    <div class="card mt-3">
        <div class="card-header">نقش ها</div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="roles table table-bordered table-striped table-hover datatable datatable-Role">
                    <thead>
                        <tr>
                            <th>عنوان</th>
                            <th style="width: 500px">دسترسی ها</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($roles as $key => $role)
                            <tr>
                                <td>
                                    {{ $role->display_name ?? '' }}
                                </td>
                                <td>
                                    @foreach ($permissions = $role->permissions()
                                                                    ->take(7)
                                                                    ->pluck('display_name')
                                                                    as $permission)
                                        <span class="badge badge-info px-2 py-1 badge-pill">{{ $permission }}</span>
                                    @endforeach
                                    @if ($permissions->count() >= 7)
                                        <span>و</span>
                                        <span class="badge badge-info px-2 py-1 badge-pill">...</span>
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-success btn-sm" href="{{ route('admin.roles.show', $role->id) }}">
                                        <span class="material-icons">
                                            remove_red_eye
                                        </span>
                                        مشاهده
                                    </a>

                                    <a class="btn btn-info btn-sm" href="{{ route('admin.roles.edit', $role->id) }}">
                                        <span class="material-icons"> create </span>
                                        ویرایش
                                    </a>

                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                        data-target="#delete-modal{{ $role->id }}">
                                        <span class="material-icons"> delete </span> حذف
                                    </button>
                                </td>
                            </tr>
                            <div id="delete-modal{{ $role->id }}" class="modal fade">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <p class="text-black typography-subheading">
                                                آیا مطمئنید؟
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-info" data-dismiss="modal" type="button">
                                                خیر
                                            </button>
                                            <form action="{{ route('admin.roles.destroy', $role->id) }}" method="POST">
                                                @method('DELETE') @csrf
                                                <input class="btn text-white bg-info" type="submit" value="بلی" />
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
                {{ $roles->onEachSide(1)->links() }}
            </div>
        </div>
    </div>
@endsection
