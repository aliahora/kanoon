@extends('admin.partials.master')
@section('admin.title',' مشاهده نقش')
@section('main')

    <div class="card">
        <div class="card-header">
            مشاهده  {{ $role->display_name  }}
        </div>

        <div class="card-body">
            <div class="mb-2">
                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <th>
                                شناسه
                            </th>
                            <td>
                                {{ $role->id }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                عنوان
                            </th>
                            <td>
                                {{ $role->display_name }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                دسترسی ها
                            </th>
                            <td>
                                @foreach ($role->permissions()
                                    ->pluck('display_name') as $permission)
                                    <span class="badge badge-info px-2 py-1 badge-pill">{{ $permission }}</span>
                                @endforeach
                            </td>
                        </tr>
                    </tbody>
                </table>
                <a style="margin-top:20px;" class="btn btn-info" href="{{ url()->previous() }}">
                    <span class="material-icons">
                        toc
                        </span>         برگشت به لیست
                </a>
            </div>
        </div>
    </div>
@endsection
