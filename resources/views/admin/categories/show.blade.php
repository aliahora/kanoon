@extends('admin.partials.master')
@section('admin.title','مشاهده دسته بندی')
@section('main')

<div class="card w-50-lg">
    <div class="card-header">
        مشاهده  {{ $category->name  }}
    </div>

    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            شناسه
                        </th>
                        <td>
                            {{ $category->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            عنوان
                        </th>
                        <td>
                           <span class="badge badge-info badge-pill p-1"> {{ $category->name }} </span>
                        </td>
                    </tr>
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-info" href="{{ url()->previous() }}">
                <span class="material-icons">
                    toc
                    </span>         برگشت به لیست
            </a>
        </div>
    </div>
</div>
@endsection
