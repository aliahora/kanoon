@extends('admin.partials.master')
@section('admin.title', 'ایجاد دسته بندی')
@section('main')

    <div class="card w-50-lg">
        <div class="card-header">
            ایجاد دسته بندی
        </div>

        <div class="card-body">
            <form action="{{ route('admin.categories.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">عنوان</label>
                    <div class="input-group">
                        <span class="mdi mdi-format-name"> </span>
                        <input type="text" name="name" value="{{ old('name') }}"
                            class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name"
                            placeholder="فارسی وارد شود" required>
                        <div class="invalid-feedback">
                            @error('name')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div>
                    <button class="btn btn-success" type="submit">
                        <span class="material-icons">
                            save
                        </span> ذخیره
                    </button>
                </div>
            </form>


        </div>
    </div>
@endsection
