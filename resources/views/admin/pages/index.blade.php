@extends('admin.partials.master')
@section('admin.title','برگه ها')
@section('main')
    <a class="btn btn-success" href="{{ route('admin.pages.create') }}">
        <span class="material-icons">
            add
        </span> افزودن برگه
    </a>
    <div class="card mt-3">
        <div class="card-header">
            برگه ها
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>
                                عنوان
                            </th>
                            <th>
                                وضعیت
                            </th>
                            <th>
                                عملیات
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pages as $key => $page)
                            <tr>
                                <td>
                                    {{ $page->title ?? '' }}
                                </td>
                                <td>
                                    @if($page->published)
                                        <span class="badge badge-success badge-pill py-1 px-2"> منتشر شده </span>
                                    @else
                                        <span class="badge badge-warning text-white badge-pill py-1 px-2"> پیشنویس </span>
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-success" href="{{ route('admin.pages.show', $page->slug) }}">
                                        <span class="material-icons">
                                            remove_red_eye
                                        </span> مشاهده
                                    </a>

                                    <a class="btn btn-sm btn-info" href="{{ route('admin.pages.edit', $page->slug) }}">
                                        <span class="material-icons">
                                            create
                                        </span> ویرایش
                                    </a>

                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                        data-target="#delete-modal{{ $page->id }}">
                                        <span class="material-icons">
                                            delete
                                        </span> حذف
                                    </button>
                                </td>
                            </tr>
                            <div id="delete-modal{{ $page->id }}" class="modal fade">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <p class="text-black typography-subheading">آیا مطمئنید؟</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-info" data-dismiss="modal" type="button">خیر</button>
                                            <form action="{{ route('admin.pages.destroy', $page->slug) }}" method="POST">
                                                @method('DELETE') @csrf
                                                <input class="btn text-white bg-info" type="submit" value="بلی">
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
