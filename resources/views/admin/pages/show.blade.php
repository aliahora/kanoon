@extends('admin.partials.master')
@section('admin.title','مشاهده برگه')
@section('main')

    <div class="card">
        <div class="card-header">
            مشاهده {{ $page->title  }}
        </div>

        <div class="card-body">
            <div class="mb-2">
                @if($page->published)
                    <span class="badge badge-success badge-pill py-1 px-2"> منتشر شده </span>
                @else
                    <span class="badge badge-warning text-white badge-pill py-1 px-2"> پیشنویس </span>
                @endif
                <br>
                <br>
                {!! $page->body !!}
                <br>
                    <br>
                <h4>چکیده</h4>
                {{$page->excert}}
                <br>
                    <br>
                <h4>کلیدواژه های متا</h4>
                {{$page->meta_keywords}}
                <br>
                    <br>
                <h4>توضیحات متا</h4>
                    {{$page->meta_description}}
                    <br>

                    <h4> ادرس صفحه -> <a href="{{ route('site.pages.show',$page)}}">لینک</a></h4>


                    <br>
                <div>
                    <a style="margin-top:20px;" class="btn btn-info" href="{{ url()->previous() }}">
                        <span class="material-icons">
                            toc
                            </span> برگشت به لیست
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
