@extends('admin.partials.master')
@section('admin.title', 'ایجاد برگه')
@section('main')

    <div class="card">
        <div class="card-header">
            ایجاد برگه
        </div>

        <div class="card-body">
            <form action="{{ route('admin.pages.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="title">عنوان</label>
                    <div class="input-group">
                        <span class="mdi mdi-format-title"> </span>
                        <input type="text" name="title" value="{{ old('title') }}"
                            class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" id="title"
                            placeholder="فارسی وارد شود" required>
                        <div class="invalid-feedback">
                            @error('title')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="mb-2" for="body"> محتوا </label>
                    <textarea name="body" id="body"
                        class="form-control {{ $errors->has('body') ? ' is-invalid' : '' }}">{{ old('body') }}</textarea>
                    <div class="invalid-feedback">
                        @error('body')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="meta_description">توضیحات متا</label>
                    <div class="input-group">
                        <span class="mdi mdi-format-title"> </span>
                        <input type="text" name="meta_description" value="{{ old('meta_description') }}"
                            class="form-control {{ $errors->has('meta_description') ? ' is-invalid' : '' }}"
                            id="meta_description" placeholder="فارسی وارد شود">
                        <div class="invalid-feedback">
                            @error('meta_description')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="meta_keywords">کلیدواژه های متا</label>
                    <div class="input-group">
                        <span class="mdi mdi-format-title"> </span>
                        <input type="text" name="meta_keywords" value="{{ old('meta_keywords') }}"
                            class="form-control {{ $errors->has('meta_keywords') ? ' is-invalid' : '' }}" id="meta_keywords"
                            placeholder="با , جدا شود">
                        <div class="invalid-feedback">
                            @error('meta_keywords')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="mb-2" for="published"> وضعیت </label>
                    <select name="published" id="published"
                        class="form-control {{ $errors->has('published') ? ' is-invalid' : '' }}">
                        <option value="1" {{ old('published') == 1 ? 'selected' : '' }}>منتشر شده</option>
                        <option value="0" {{ old('published') == 0 ? 'selected' : '' }}>پیشنویس</option>
                    </select>
                    <div class="invalid-feedback">
                        @error('published')
                        {{ $message }}
                        @enderror
                    </div>
                </div>

                <div>
                    <button class="btn btn-success" type="submit">
                        <span class="material-icons">
                            save
                        </span> ذخیره
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
