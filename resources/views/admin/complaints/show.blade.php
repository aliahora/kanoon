@extends('admin.partials.master')
@section('admin.title', ' مشاهده شکایت')
@section('main')

    <div class="card w-50-lg">
        <div class="card-header">
            مشاهده شکایت
        </div>

        <div class="card-body" style="line-height: 2;">
            <div class="mb-2">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card-title">مشخصات موکل</div>
                        <div>نام و نام خانوادگی: {{ $complaint->shaki_name }}</div>
                        <div>کد ملی: {{ $complaint->kode_meli }}</div>
                        <div>شماره تلفن: {{ $complaint->shaki_tel }}</div>
                        <div>آدرس: {{ $complaint->shaki_address }}</div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card-title">مشخصات وکیل</div>
                        <div>نام و نام خانوادگی: {{ $complaint->vakil_name }}</div>
                        <div>شماره تلفن: {{ $complaint->vakil_tel }}</div>
                        <div>آدرس: {{ $complaint->vakil_address }}</div>
                    </div>
                </div>

                <br>
                <div>مدارک:</div>
                @forelse ($complaint->attachments as $attachment)
                    {{ $loop->iteration . '- ' }} <a target="_blank" href="{{ route('admin.attachment', $attachment->filename) }}">فایل</a><br>
                @empty
                    <div>مدرکی بارگزاری نشده است.</div>
                @endforelse

                <br>
                <div>متن شکایت:</div>
                <div>{{ $complaint->matn }}</div>
                <a style="margin-top:20px;" class="btn btn-info" href="{{ url()->previous() }}">
                    <span class="material-icons">
                        toc
                    </span> برگشت به لیست
                </a>
            </div>
        </div>
    </div>
@endsection
