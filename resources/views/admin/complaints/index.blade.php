@extends('admin.partials.master')
@section('admin.title', 'شکایات')
@section('main')
    <div class="card mt-3">
        <div class="card-header d-flex justify-content-between">
            <div class="align-self-center">شکایات</div>
            <div class="search w-25">
                <form action="{{ route('admin.complaints.search') }}" method="GET">
                    <div class="has-search fg--search">
                        <button type="submit"><span class="mdi mdi-magnify form-control-feedback"></span></button>
                        <input name="q" value="{{ request()->input('q') ?? '' }}" type="text" class="form-control" placeholder="کد پیگیری">
                    </div>

                </form>

            </div>

        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>
                                کد پیگیری
                            </th>
                            <th>
                                نام شاکی
                            </th>
                            <th>
                                تلفن شاکی
                            </th>
                            <th>
                                وضعیت
                            </th>
                            <th>
                                تاریخ ایجاد
                            </th>
                            <th>
                                عملیات
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($complaints as $key => $complaint)
                            <tr>
                                <td>
                                    <div class="text-truncate {{ $complaint->readed ? '' : 'font-weight-bold' }}">
                                        {{ $complaint->code ?? '' }}
                                    </div>
                                </td>
                                <td>
                                    {{ $complaint->shaki_name ?? '' }}
                                </td>
                                <td>
                                    {{ $complaint->shaki_tel ?? '' }}
                                </td>
                                <td>
                                    @switch($complaint->status)
                                        @case(App\Complaint::CREATED_STATUS)
                                        <span class="badge badge-info badge-pill py-1 px-2"> ثبت شده </span>
                                        @break
                                        @case(App\Complaint::PENDING_STATUS)
                                        <span class="badge badge-warning text-white badge-pill py-1 px-2"> در حال پیگیری </span>
                                        @break
                                        @case(App\Complaint::DENIED_STATUS)
                                        <span class="badge badge-danger badge-pill py-1 px-2"> رد شده </span>
                                        @break
                                        @case(App\Complaint::ACCEPTED_STATUS)
                                        <span class="badge badge-success badge-pill py-1 px-2"> پذیرفته شده </span>
                                        @break
                                        @case(App\Complaint::PROBLEM_STATUS)
                                        <span class="badge badge-danger badge-pill py-1 px-2"> اطلاعات ناقص </span>
                                        @break
                                        @case(App\Complaint::FINISHED_STATUS)
                                        <span class="badge badge-success badge-pill py-1 px-2"> خاتمه یافته </span>
                                        @break
                                    @endswitch
                                </td>
                                <td>
                                    {{ verta($complaint->created_at)->format('j F Y | H:i') ?? '' }}
                                </td>
                                <td>

                                    <a class="btn btn-float btn-sm btn-success"
                                        href="{{ route('admin.complaints.show', $complaint) }}" data-tooltip="tooltip"
                                        data-placement="bottom" title="مشاهده">
                                        <span class="material-icons">
                                            remove_red_eye
                                        </span>
                                    </a>

                                    <button class="btn btn-float btn-sm btn-info" data-tooltip="tooltip" data-toggle="modal"
                                        data-target="#edit-modal{{ $complaint->id }}" data-placement="bottom"
                                        title="تغییر وضعیت">
                                        <span class="material-icons">
                                            create
                                        </span>
                                    </button>

                                    <button type="button" class="btn btn-float btn-danger btn-sm" data-toggle="modal"
                                        data-target="#delete-modal{{ $complaint->id }}" data-tooltip="tooltip"
                                        data-placement="bottom" title="حذف">
                                        <span class="material-icons">
                                            delete
                                        </span>
                                    </button>
                                </td>
                            </tr>
                            <div id="delete-modal{{ $complaint->id }}" class="modal fade">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <p class="text-black typography-subheading">آیا مطمئنید؟</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-info" data-dismiss="modal" type="button">خیر</button>
                                            <form action="{{ route('admin.complaints.destroy', $complaint->id) }}"
                                                method="POST">
                                                @method('DELETE') @csrf
                                                <input class="btn text-white bg-info" type="submit" value="بلی">
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="edit-modal{{ $complaint->id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <form action="{{ route('admin.complaints.update', $complaint) }}" method="POST">
                                        @method('PUT') @csrf
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <p class="text-black typography-subheading">تغییر وضعیت</p>
                                                <div class="form-group">
                                                    <label class="mb-1" for="category_id"> وضعیت </label>
                                                    <select name="status" id="status"
                                                        class="form-control {{ $errors->has('status') ? ' is-invalid' : '' }}"
                                                        required>
                                                        @foreach (App\Complaint::STATUSES_ARRAY as $key => $status)
                                                            <option value="{{ $status }}"
                                                                {{ $complaint->status == $status ? 'selected' : '' }}>
                                                                {{ $key }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        @error('status')
                                                        {{ $message }}
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-info" data-dismiss="modal"
                                                    type="button">انصراف</button>
                                                <input class="btn text-white bg-info" type="submit" value="بروزرسانی">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
                {{ $complaints->onEachSide(1)->links() }}
            </div>


        </div>
    </div>
@endsection
