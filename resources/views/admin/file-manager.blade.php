@extends('admin.partials.master')
@section('admin.title','مدیریت فایل')
@section('main')
<iframe class="w-100" style="border: none; height: 500px;" src="{{ route('admin.unisharp.lfm.show',['type' => 'image']) }}"></iframe>
@endsection
