@extends('admin.partials.master')
@section('admin.title', 'پیام ها')
@section('main')
    <div class="card mt-3">
        <div class="card-header">
            پیام ها
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>
                                متن
                            </th>
                            <th>
                                تاریخ ایجاد
                            </th>
                            <th>
                                عملیات
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($messages as $key => $message)
                            <tr>
                                <td>
                                <div class="text-truncate {{ $message->readed ? '' : 'font-weight-bold' }}" style="width: 600px;">
                                        {{ $message->body ?? '' }}
                                    </div>
                                </td>
                                <td>
                                    {{ verta($message->created_at)->format('j F Y | H:i') ?? '' }}
                                </td>
                                <td>
                                    <a class="btn btn-float btn-sm btn-success"
                                        href="{{ route('admin.messages.show', $message) }}" data-tooltip="tooltip"
                                        data-placement="bottom" title="مشاهده">
                                        <span class="material-icons">
                                            remove_red_eye
                                        </span>
                                    </a>
                                    <button type="button" class="btn btn-float btn-danger btn-sm" data-toggle="modal"
                                        data-target="#delete-modal{{ $message->id }}" data-tooltip="tooltip"
                                        data-placement="bottom" title="حذف">
                                        <span class="material-icons">
                                            delete
                                        </span>
                                    </button>
                                </td>
                            </tr>
                            <div id="delete-modal{{ $message->id }}" class="modal fade">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <p class="text-black typography-subheading">آیا مطمئنید؟</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-info" data-dismiss="modal" type="button">خیر</button>
                                            <form action="{{ route('admin.messages.destroy', $message->id) }}"
                                                method="POST">
                                                @method('DELETE') @csrf
                                                <input class="btn text-white bg-info" type="submit" value="بلی">
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
                {{ $messages->onEachSide(1)->links() }}
            </div>


        </div>
    </div>
@endsection
