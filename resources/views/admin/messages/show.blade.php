@extends('admin.partials.master')
@section('admin.title',' مشاهده پیام')
@section('main')

    <div class="card w-50-lg">
        <div class="card-header">
            مشاهده  پیام
        </div>

        <div class="card-body">
            <div class="mb-2">
                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <th>
                                نام
                            </th>
                            <td>
                                {{ $message->name }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                ایمیل
                            </th>
                            <td>
                                {{ $message->email ?? 'بدون ایمیل' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                موضوع
                            </th>
                            <td>
                                {{ $message->subject }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                متن
                            </th>
                            <td>
                                {{ $message->body }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                تاریخ ایجاد
                            </th>
                            <td>
                                {{ verta($message->created_at)->format('j F Y | H:i') ?? '' }}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <a style="margin-top:20px;" class="btn btn-info" href="{{ url()->previous() }}">
                    <span class="material-icons">
                        toc
                        </span>         برگشت به لیست
                </a>
            </div>
        </div>
    </div>
@endsection
