@extends('admin.partials.master')
@section('admin.title','ساخت کاربر')
@section('main')

    <div class="card w-50-lg">
        <div class="card-header">
            ساخت کاربر
        </div>

        <div class="card-body">
            <form action="{{ route('admin.users.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">نام</label>
                    <div class="input-group">
                        <span class="mdi mdi-account"> </span>
                        <input type="text" name="name"
                        value="{{ old('name') }}"
                            class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name"
                            aria-describedby="emailHelp" placeholder="فلان فلانی" autofocus required>
                        <div class="invalid-feedback">
                            @error('name')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">آدرس الکترونیکی</label>
                    <div class="input-group">
                        <span class="mdi mdi-email"> </span>
                        <input type="email" name="email"
                        value="{{ old('email') }}"
                            class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email"
                            aria-describedby="emailHelp" placeholder="example@gmail.com" required>
                        <div class="invalid-feedback">
                            @error('email')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password">گذرواژه</label>
                    <div class="input-group">
                        <span class="mdi mdi-account-key"> </span>
                        <input id="password" type="password"
                            class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                            placeholder="********" required>
                        <div class="invalid-feedback">
                            @error('password')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="mb-2" for="role"> نقش </label>
                    <select name="role" id="role" class="form-control {{ $errors->has('role') ? ' is-invalid' : '' }}" required>
                        @foreach ($roles as $role)
                            <option value="{{ $role->name }}" {{ old('role') == $role->name ? 'selected' : '' }}>{{ $role->display_name }}</option>
                        @endforeach
                    </select>
                    <div class="invalid-feedback">
                        @error('role')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
                <div>
                    <button class="btn btn-success" type="submit">
                        <span class="material-icons">
                            save
                        </span> ذخیره
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
