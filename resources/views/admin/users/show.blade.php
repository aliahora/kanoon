@extends('admin.partials.master')
@section('admin.title','مشاهده کاربر')
@section('main')

    <div class="card w-50-lg">
        <div class="card-header">
            مشاهده  {{ $user->name  }}
        </div>
        <div class="card-body">
            <div class="mb-2">
                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <th>
                                شناسه
                            </th>
                            <td>
                                {{ $user->id }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                نام
                            </th>
                            <td>
                                {{ $user->name }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                ادرس الکترونیکی
                            </th>
                            <td>
                                {{ $user->email }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                نقش ها
                            </th>
                            <td>
                                @foreach ($user->roles()
                                    ->pluck('display_name') as $role)
                                    <span class="badge badge-info px-2 py-1 badge-pill">{{ $role }}</span>
                                @endforeach
                            </td>
                        </tr>
                    </tbody>
                </table>
                <a style="margin-top:20px;" class="btn btn-info" href="{{ route('admin.users.index') }}">
                    <span class="material-icons">
                        toc
                        </span>        برگشت به لیست
                </a>
            </div>


        </div>
    </div>
@endsection
