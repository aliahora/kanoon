@extends('admin.partials.master')
@section('admin.title','کاربران')
@section('main')

    <a class="btn btn-success" href="{{ route('admin.users.create') }}">
        <span class="material-icons">
        add
        </span> افزودن کاربر
    </a>
    <div class="mt-3 card">
        <div class="card-header">
            کاربران
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>
                                نام
                            </th>
                            <th>
                                ایمیل
                            </th>
                            <th>
                                نقش
                            </th>
                            <th>
                                عملیات
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $key => $user)
                            <tr>
                                <td>
                                    {{ $user->name ?? '' }}
                                </td>
                                <td>
                                    {{ $user->email ?? '' }}
                                </td>
                                <td>
                                    @foreach ($user->roles()->pluck('display_name') as $role)
                                        <span class="badge badge-info px-2 py-1 badge-pill">{{ $role }}</span>
                                    @endforeach
                            </td>
                            <td>
                                <a class="btn btn-sm btn-success" href="{{ route('admin.users.show', $user->id) }}">
                                    <span class="material-icons">
                                        remove_red_eye
                                        </span>        مشاهده
                                </a>

                                <a class="btn btn-sm btn-info" href="{{ route('admin.users.edit', $user->id) }}">
                                    <span class="material-icons">
                                        create
                                        </span>       ویرایش
                                </a>

                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                    data-target="#delete-modal{{ $user->id }}">
                                    <span class="material-icons">
                                        delete
                                        </span>              حذف
                                </button>
                            </td>
                            </tr>
                            <div id="delete-modal{{ $user->id }}" class="modal fade">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <p class="text-black typography-subheading">آیا مطمئنید؟</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-info" data-dismiss="modal" type="button">خیر</button>
                                            <form action="{{ route('admin.users.destroy', $user->id) }}" method="POST">
                                                @method('DELETE') @csrf
                                                <input class="btn text-white bg-info" type="submit" value="بلی">
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
                {{ $users->onEachSide(1)->links() }}

            </div>


        </div>
    </div>


@endsection
