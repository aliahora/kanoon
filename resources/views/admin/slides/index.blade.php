@extends('admin.partials.master')
@section('admin.title', 'اسلاید ها')
@section('main')
    <a class="btn btn-success" href="{{ route('admin.slides.create') }}">
        <span class="material-icons">
            add
        </span> افزودن اسلاید
    </a>
    <div class="card mt-3">
        <div class="card-header">
            اسلاید ها
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>
                                تصویر
                            </th>
                            <th>
                                آدرس
                            </th>
                            <th>
                                توضیح
                            </th>
                            <th>
                                ترتیب
                            </th>
                            <th>
                                عملیات
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($slides as $key => $slide)
                            <tr>
                                @if ($slide->article_id)
                                    <td>
                                        <img width="60px" src="{{ thumb($slide->article->image) }}" alt="عکس">
                                    </td>
                                    <td>
                                        <a class="btn btn-info btn-sm" target="_blank"
                                            href="{{ route('site.articles.show', $slide->article) }}">
                                            <span class="material-icons">
                                                open_in_new
                                            </span>
                                            بازکردن
                                        </a>
                                    </td>
                                    <td>
                                        {{ $slide->article->title ?? '' }}
                                    </td>
                                @else
                                    <td>
                                        <img width="60px" src="{{ thumb($slide->image) }}" alt="عکس">
                                    </td>
                                    <td>
                                        <a class="btn btn-info btn-sm" target="_blank" href="{{ $slide->link }}">
                                            <span class="material-icons">
                                                open_in_new
                                            </span>
                                            بازکردن
                                        </a>
                                    </td>
                                    <td>
                                        {{ $slide->caption ?? '' }}
                                    </td>
                                @endif
                                <td>
                                    {{ $slide->order }}
                                </td>
                                <td>

                                    <a class="btn btn-sm btn-info" href="{{ route('admin.slides.edit', $slide->id) }}">
                                        <span class="material-icons">
                                            create
                                        </span> ویرایش
                                    </a>

                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                        data-target="#delete-modal{{ $slide->id }}">
                                        <span class="material-icons">
                                            delete
                                        </span> حذف
                                    </button>
                                </td>
                            </tr>
                            <div id="delete-modal{{ $slide->id }}" class="modal fade">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <p class="text-black typography-subheading">آیا مطمئنید؟</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-info" data-dismiss="modal" type="button">خیر</button>
                                            <form action="{{ route('admin.slides.destroy', $slide->id) }}" method="POST">
                                                @method('DELETE') @csrf
                                                <input class="btn text-white bg-info" type="submit" value="بلی">
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
            </div>


        </div>
    </div>
@endsection
