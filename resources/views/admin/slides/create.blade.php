@extends('admin.partials.master')
@section('admin.title', 'ایجاد اسلاید')
@section('main')

    <div class="card w-50-lg">
        <div class="card-header">
            ایجاد اسلاید
        </div>

        <div class="card-body">
            <form action="{{ route('admin.slides.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label class="mr-1" for="caption">توضیح</label>
                    <div class="input-group">
                        <span class="mdi mdi-format-name"> </span>
                        <textarea class="form-control mt-1 {{ $errors->has('caption') ? ' is-invalid' : '' }}"
                            name="caption" id="caption" placeholder="فارسی وارد شود">{{ old('caption') }}</textarea>
                        <div class="invalid-feedback">
                            @error('caption')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="mr-1" for="link">آدرس</label>
                    <div class="input-group">
                        <span class="mdi mdi-format-name"> </span>
                        <input type="url" name="link" value="{{ old('link') }}"
                            class="form-control {{ $errors->has('link') ? ' is-invalid' : '' }}" id="link"
                            placeholder="مثال http://google.com">
                        <div class="invalid-feedback">
                            @error('link')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="mr-1" for="image">تصویر</label>
                    <div class="input-group">
                        <span class="input-group-btn">
                            <a id="lfm" data-input="image" data-preview="holder" class="btn btn-info text-white">
                                <span class="material-icons">insert_photo</span> انتخاب
                            </a>
                        </span>
                        <input placeholder="تصویر را انتخاب کنید" id="image" value="{{ old('image') }}"
                            class="form-control mt-1 {{ $errors->has('image') ? ' is-invalid' : '' }}" type="text"
                            name="image" readonly>
                        <div class="invalid-feedback">
                            @error('image')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="my-3">یا</div>

                <div class="form-group">
                    <label class="mr-1" for="article_id">انتخاب خبر</label>

                    <span class="mdi mdi-format-name"> </span>
                    <select class="form-control mt-1 {{ $errors->has('article_id') ? ' is-invalid' : '' }}"
                        name="article_id" id="article_id">
                        <option></option>
                        @foreach ($articles as $article)
                            <option value="{{ $article->id }}" {{ old('article_id') == $article->id ? 'selected' : '' }}>
                                {{ $article->title }}</option>
                        @endforeach
                    </select>
                    <div class="invalid-feedback">
                        @error('article_id')
                        {{ $message }}
                        @enderror
                    </div>
                </div>

                <div class="mt-5 form-group">
                    <label for="order">ترتیب</label>
                    <div class="input-group">
                        <span class="mdi mdi-format-url"> </span>
                        <input type="number" name="order" value="{{ old('order') }}"
                            class="form-control {{ $errors->has('order') ? ' is-invalid' : '' }}" id="order"
                            placeholder="1 یا 2 یا 3 ..." required>
                        <div class="invalid-feedback">
                            @error('order')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>

                <div>
                    <button class="btn btn-success" type="submit">
                        <span class="material-icons">
                            save
                        </span> ذخیره
                    </button>
                </div>
            </form>


        </div>
    </div>
@endsection
