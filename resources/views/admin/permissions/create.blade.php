@extends('admin.partials.master')
@section('admin.title', 'ساخت دسترسی')
@section('main')

    <div class="card w-50-lg">
        <div class="card-header">
            ساخت دسترسی
        </div>

        <div class="card-body">
            <form action="{{ route('admin.permissions.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">نام سیستمی</label>
                    <div class="input-group">
                        <span class="mdi mdi-format-title"> </span>
                        <input type="text" name="name" value="{{ old('name') }}"
                            class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name"
                            placeholder="انگلیسی وارد شود" autofocus required>
                        <div class="invalid-feedback">
                            @error('name')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="display_name">نام نمایشی</label>
                    <div class="input-group">
                        <span class="mdi mdi-format-title"> </span>
                        <input type="text" name="display_name" value="{{ old('display_name') }}"
                            class="form-control {{ $errors->has('display_name') ? ' is-invalid' : '' }}" id="display_name"
                            placeholder="فارسی وارد شود" required>
                        <div class="invalid-feedback">
                            @error('display_name')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div>
                    <button class="btn btn-success" type="submit">
                        <span class="material-icons">
                            save
                        </span> ذخیره
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
