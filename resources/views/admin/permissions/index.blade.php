@extends('admin.partials.master')
@section('admin.title','دسترسی ها')
@section('main')
    <a class="btn btn-success" href="{{ route('admin.permissions.create') }}">
        <span class="material-icons">
            add
            </span> افزودن دسترسی
    </a>
    <div class="card mt-3">
        <div class="card-header">
            دسترسی ها
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>
                                عنوان
                            </th>
                            <th>
                                عملیات
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($permissions as $key => $permission)
                            <tr>
                                <td>
                                    {{ $permission->display_name ?? '' }}
                                </td>
                                <td>

                                    <a class="btn btn-sm btn-info"
                                        href="{{ route('admin.permissions.edit', $permission->id) }}">
                                        <span class="material-icons">
                                            create
                                            </span>      ویرایش
                                    </a>

                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                        data-target="#delete-modal{{ $permission->id }}">
                                        <span class="material-icons">
                                            delete
                                            </span>     حذف
                                    </button>
                                </td>
                            </tr>
                            <div id="delete-modal{{ $permission->id }}" class="modal fade">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <p class="text-black typography-subheading">آیا مطمئنید؟</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-info" data-dismiss="modal" type="button">خیر</button>
                                            <form action="{{ route('admin.permissions.destroy', $permission->id) }}"
                                                method="POST">
                                                @method('DELETE') @csrf
                                                <input class="btn text-white bg-info" type="submit" value="بلی">
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
                {{ $permissions->onEachSide(1)->links() }}
            </div>


        </div>
    </div>
@endsection
