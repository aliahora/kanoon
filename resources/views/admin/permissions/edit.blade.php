@extends('admin.partials.master')
@section('admin.title','ویرایش دسترسی')
@section('main')

    <div class="card w-50-lg">
        <div class="card-header">
            ویرایش {{ $permission->display_name }}
        </div>

        <div class="card-body">
            <form action="{{ route('admin.permissions.update', [$permission->id]) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">نام سیستمی</label>
                    <div class="input-group">
                        <span class="mdi mdi-format-title"> </span>
                    <input type="text" name="name" value="{{ $permission->name }}"
                            class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name"
                            placeholder="انگلیسی وارد شود" autofocus required>
                        <div class="invalid-feedback">
                            @error('name')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="display_name">نام نمایشی</label>
                    <div class="input-group">
                        <span class="mdi mdi-format-title"> </span>
                        <input type="text" name="display_name" value="{{ $permission->display_name }}"
                            class="form-control {{ $errors->has('display_name') ? ' is-invalid' : '' }}" id="display_name"
                            placeholder="فارسی وارد شود" required>
                        <div class="invalid-feedback">
                            @error('display_name')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div>
                    <button class="btn btn-success" type="submit">
                        <span class="material-icons">
                            save
                            </span> بروزرسانی
                    </button>
                </div>
            </form>


        </div>
    </div>
@endsection
