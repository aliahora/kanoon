<div class="modal-content" xmlns:wire="http://www.w3.org/1999/xhtml">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ورود به ناحیه کاربری</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <form wire:submit.prevent="loginUser">
            @include('layouts.flash-message')
            <div class="form-group">
                <label for="login_email">آدرس الکترونیکی</label>
                <div class="input-group">
                    <span class="mdi mdi-email"> </span>
                    <input wire:model="email" type="text" name="login"
                        class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="login_email"
                        aria-describedby="emailHelp">
                    <div class="invalid-feedback">
                        @error('email')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="login_password">گذرواژه</label>

                <div class="input-group">
                    <span class="mdi mdi-account-key"> </span>
                    <input wire:model="password" id="login_password" type="password"
                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                        placeholder="********" />
                    <div class="invalid-feedback">
                        @error('password')
                        {{ $message }}
                        @enderror
                    </div>
                </div>

            </div>
            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-info">
                        ورود
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
