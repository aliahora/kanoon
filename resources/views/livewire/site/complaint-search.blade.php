<div class="modal-content" xmlns:wire="http://www.w3.org/1999/xhtml">
    <div class="modal-header">
        <h5 class="modal-title">پیگیری شکایات</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">

        <div wire:loading class="w-100">
            <div class="alert alert-info fade show" role="alert">
                <span class="material-icons">
                    hourglass_empty
                </span>
                لطفا شکیبا باشید...
            </div>
        </div>

        <div wire:loading.remove>
            @include('layouts.flash-message')
        </div>

        <form wire:submit.prevent="search">
            <div class="input-group">
                <input type="number" class="form-control {{ $errors->has('code') ? ' is-invalid' : '' }}"
                    title="کد پیگیری" wire:model.defer="code" name="code" placeholder="کد پیگیری" required>
                <div class="invalid-feedback">
                    @error('code')
                    {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group mt-4 w-50">
                <label class="mr-1" for="captcha">کد امنیتی</label>
                <div class="input-group">
                    <input name="captcha" id="captcha" wire:model.defer="captcha"
                        class="form-control {{ $errors->has('captcha') ? ' is-invalid' : '' }}" required>
                    <img src="{{ captcha_src('flat') }}" alt="">
                    <div class="invalid-feedback">
                        @error('captcha')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
                <input type="submit" class="btn text-white bg-info mt-4" value="ارسال">
            </div>
        </form>
    </div>

</div>
