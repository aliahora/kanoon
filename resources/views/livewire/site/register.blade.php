<div class="modal-content" xmlns:wire="http://www.w3.org/1999/xhtml">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">عضویت</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <form wire:submit.prevent="saveUser">
            <div class="form-group">
                <label for="name">نام</label>
                <div class="input-group">
                    <span class="mdi mdi-account"> </span>
                    <input wire:model="name" type="text" name="name"
                        class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name"
                        aria-describedby="emailHelp" placeholder="فلان فلانی" autofocus>
                    <div class="invalid-feedback">
                        @error('name')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="email">آدرس الکترونیکی</label>
                <div class="input-group">
                    <span class="mdi mdi-email"> </span>
                    <input wire:model="email" type="email" name="email"
                        class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email"
                        aria-describedby="emailHelp" placeholder="example@gmail.com">
                    <div class="invalid-feedback">
                        @error('email')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="password">گذرواژه</label>
                <div class="input-group">
                    <span class="mdi mdi-account-key"> </span>
                    <input wire:model="password" id="password" type="password"
                        class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                        placeholder="********" />
                    <div class="invalid-feedback">
                        @error('password')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="password_cfm">تایید گذرواژه</label>
                <div class="input-group">
                    <span class="mdi mdi-account-key"> </span>
                    <input wire:model="password_confirmation" id="password_cfm" type="password"
                        class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                        name="password_confirmation" placeholder="********" />
                    <div class="invalid-feedback">
                        @error('password_confirmation')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-info">
                        ثبت نام
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
