<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <link href="{{ asset('css/material.min.css') }}" rel="stylesheet">
</head>

<body>
    <div class="container-fluid d-flex flex-column justify-content-center align-items-center" style="height: 100vh;">
        <div class="typography-display-4">
            @yield('code')
        </div>

        <div class="typography-display-1">
            @yield('message')
        </div>
        <div class="d-flex mt-4">
            <a class="btn btn-info m-1" href="{{ url()->previous() }}">بازگشت</a>
            <a class="btn btn-info m-1" href="{{ route('site.home') }}">صفحه اصلی</a>
        </div>

    </div>
</body>

</html>
