@if (session()->has('msg') )
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <span class="material-icons">
            check_circle_outline
        </span>
        {{ session('msg') }}

    </div>
@endif

@if (session()->has('err') )

    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <span class="material-icons">
             error_outline
        </span>
        {{ session('err') }}

    </div>
@endif
