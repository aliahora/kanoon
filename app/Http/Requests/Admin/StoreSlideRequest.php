<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreSlideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order' => 'required|numeric',
            'caption' => 'required_without:article_id',
            'image' => 'required_without:article_id',
            'link' => 'required_without:article_id',
            'article_id' => 'required_without_all:caption,image,link',
        ];
    }
}
