<?php

namespace App\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;

class StoreComlaintRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'captcha' => 'required|captcha',
            'shaki_name' => 'required',
            'kode_meli' => 'required|numeric',
            'shaki_address' => 'required',
            'vakil_name' => 'required',
            'vakil_address' => 'required',
            'shaki_tel' => 'required|numeric',
            'vakil_tel' => 'required|numeric',
            'matn' => 'required',
        ];
    }
}
