<?php

namespace App\Http\Livewire\Site;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Login extends Component
{
    public $email;
    public $password;

    // public function updated($field)
    // {
    //     $this->validateOnly($field, [
    //         'email' => 'required|email',
    //         'password' => 'required|min:6',
    //     ]);

    // }

    public function loginUser()
    {
        $validatedData = $this->validate([
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        $loginTry = Auth::attempt($validatedData);

        if ($loginTry) {

            /**
             * @var \App\User $user
             */

            $user = auth()->user();

            if (!$user->roles()->pluck('name')->contains('user'))
                $this->redirect('/admin');
            else
                $this->redirect('/');
        } else {
            session()->flash('err', 'اطلاعات وارد شده اشتباه است');
        }
    }

    public function render()
    {
        return view('livewire.site.login');
    }
}
