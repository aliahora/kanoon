<?php

namespace App\Http\Livewire\Site;

use App\Complaint;
use Livewire\Component;

class ComplaintSearch extends Component
{
    public $code;
    public $captcha;

    public function search()
    {
        $this->validate([
            'code' => 'required',
            'captcha' => 'captcha',
        ]);

        $peygiri = Complaint::where('code', $this->code)->first();

        if ($peygiri) {
            session()->flash('msg', 'وضعیت شکایت شما: ' . Complaint::translateStatus($peygiri->status));
        } else {
            session()->flash('err', 'کد پیگیری وارد شده اشتباه است');
        }
    }

    public function render()
    {
        return view('livewire.site.complaint-search');
    }
}
