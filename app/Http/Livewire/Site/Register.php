<?php

namespace App\Http\Livewire\Site;

use App\User;
use Illuminate\Validation\ValidationException;
use Livewire\Component;

class Register extends Component
{
    public $name;
    public $email;
    public $password;
    public $password_confirmation;

    public function updated($field)
    {
            $this->validateOnly($field, [
                'name' => 'required|min:6',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6',
                'password_confirmation' => 'required|same:password',
            ]);

    }

    public function saveUser()
    {
        $validatedData = $this->validate([
            'name' => 'required|min:6',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password',
        ]);

        $validatedData['password'] = bcrypt($validatedData['password']);

        User::create($validatedData);

        $this->redirect('/');
    }

    public function render()
    {
        return view('livewire.site.register');
    }
}
