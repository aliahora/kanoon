<?php

namespace App\Http\Controllers\Site;

use App\Article;
use App\Http\Controllers\Controller;
use App\Slide;
use App\Tool;

class HomeController extends Controller
{

    public function index()
    {
        $tools = Tool::orderBy('order', 'asc')->get();
        $slides = Slide::orderBy('order', 'asc')->get();
        $articles = Article::where("published","=","1")->latest()->take(8)->get();
        return view('site.home.index', compact('tools','slides','articles'));
    }
}
