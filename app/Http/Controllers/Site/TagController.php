<?php

namespace App\Http\Controllers\Site;

use App\Article;
use App\Http\Controllers\Controller;
use App\Tag;

class TagController extends Controller
{
    public function show(Tag $tag)
    {
        return Article::withAnyTags([$tag->name])->get();
    }
}
