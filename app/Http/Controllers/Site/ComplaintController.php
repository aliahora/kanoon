<?php

namespace App\Http\Controllers\Site;

use App\Attachment;
use App\Complaint;
use App\Http\Controllers\Controller;
use App\Http\Requests\Site\StoreComlaintRequest;
use Illuminate\Http\Request;

class ComplaintController extends Controller
{
    public function create()
    {
        return view('site.complaint.store');
    }

    public function store(StoreComlaintRequest $request)
    {
        $data = $request->all();
        $data['code'] = generate_complaint_code();

        $valid_extensions = [
            'jpg',
            'jpeg',
            'png',
            'pdf',
            'doc',
            'docx'
        ];

        if ($request->hasFile('attachments')) {
            foreach ($request->file('attachments') as $file) {

                $size = ($file->getSize() / 1024);
                $extension = $file->extension();

                if (!in_array($extension, $valid_extensions)) {
                    return redirect()->route('site.complaint')->with('err', 'فرمت فایل انتخابی مجاز نمی باشد.');
                }

                if ($size > 700) {
                    return redirect()->route('site.complaint')->with('err', 'فایل شما بیشتر از 700 کیلوبایت می باشد.');
                }

                $file->store('attachments');
                $complaint = Complaint::create($data);
                $complaint->attachments()->save(new Attachment(['filename' => $file->hashName()]));
            }

        } else {
            $complaint = Complaint::create($data);
        }

        return redirect()->route('site.complaint')->with('msg', 'شکایت شما با موفقیت ثبت شد.' . ' کد پیگیری شما ' . $complaint->code . ' می باشد.');
    }

}
