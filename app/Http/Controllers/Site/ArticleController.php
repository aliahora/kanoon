<?php

namespace App\Http\Controllers\Site;

use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::where("published","=","1")->latest()->paginate(10);
        return view('site.article.index', compact('articles'));
    }

    public function show(Article $article)
    {
        $article->sortedComments('desc');
        return view('site.article.show', compact('article'));
    }

    public function search(Request $request)
    {
        $articles = Article::where('title', 'LIKE', "%$request->q%")->latest()->paginate(10);
        $articles->appends(['q' => $request->q]);
        return view('site.article.index', compact('articles'));
    }
}
