<?php

namespace App\Http\Controllers\Site;

use App\Article;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Http\Requests\site\StoreCommentRequest;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(StoreCommentRequest $request,Article $article)
    {
        $data =  $request->all();
        $data['article_id'] = $article->id;
        $comment = Comment::create($data);
        if ($comment){
            return redirect()->route('site.articles.show',$article)->with(['msg' => 'نظر شما با موفقیت ثبت شد']);
        }
        return redirect()->route('site.articles.show',$article)->with(['msg' => 'نظر شما ثبت نشد']);
    }

}
