<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Complaint;
use App\Http\Controllers\Controller;
use App\Message;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $comments = Comment::whereDate('created_at', today())->get();
        $messages = Message::whereDate('created_at', today())->get();
        $complaints = Complaint::whereDate('created_at', today())->get();
        return view('admin.home', compact('complaints','messages','comments'));
    }
}
