<?php

namespace App\Http\Controllers\Admin;

use App\Message;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index()
    {
        $this->authorize('view', Message::class);
        $messages = Message::latest()->paginate(10);

        return view('admin.messages.index', compact('messages'));
    }

    /**
     * Remove Message from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        $this->authorize('delete', $message);

        $message->delete();

        return redirect()->route('admin.messages.index')->with(['msg' => 'پیام با موفقیت حذف شد']);
    }

    public function show(Message $message)
    {
        $this->authorize('view', $message);

        $message->readed = true;
        $message->save();

        return view('admin.messages.show', compact('message'));
    }

    /**
     * Delete all selected Message at once.
     *
     * @param Request $request
     */
    public function massDestroy()
    {
        Message::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }
}
