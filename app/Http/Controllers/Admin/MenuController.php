<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreMenuRequest;
use App\Http\Requests\Admin\UpdateMenuRequest;
use App\Menu;
use App\Page;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function index()
    {
        $this->authorize('view',Menu::class );
        $pages = Page::all();
        $menus = Menu::orderBy('order')->get()->toTree();
        $allMenus = Menu::all();

        return view('admin.menus.index', compact('menus', 'allMenus', 'pages'));
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMenuRequest $request)
    {
        $this->authorize('create', Menu::class);

        Menu::create($request->all());

        return redirect()->route('admin.menus.index')->with(['msg' => 'منو با موفقیت اضافه شد']);
    }

    /**
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMenuRequest $request, Menu $menu)
    {
        $this->authorize('update', $menu);

        $menu->update($request->all());

        return redirect()->route('admin.menus.index')->with(['msg' => 'منو با موفقیت ویرایش شد']);
    }


    /**
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $this->authorize('delete', $menu);

        $menu->delete();

        return redirect()->route('admin.menus.index')->with(['msg' => 'منو با موفقیت حذف شد']);
    }

    /**
     *
     * @param Request $request
     */
    public function massDestroy()
    {
        Menu::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }
}
