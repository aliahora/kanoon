<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StorePageRequest;
use App\Http\Requests\Admin\UpdatePageRequest;
use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        $this->authorize('view',Page::class );
        $pages = Page::paginate(10);

        return view('admin.pages.index', compact('pages'));
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Page::class);

        return view('admin.pages.create');
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StorePageRequest $request)
    {
        $this->authorize('create', Page::class);

        Page::create($request->all());

        return redirect()->route('admin.pages.index')->with(['msg' => 'برگه با موفقیت اضافه شد']);
    }


    /**
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        $this->authorize('update', $page);

        return view('admin.pages.edit', compact('page'));
    }

    /**
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePageRequest $request, Page $page)
    {
        $this->authorize('update', $page);

        $page->update($request->all());

        return redirect()->route('admin.pages.index')->with(['msg' => 'برگه با موفقیت ویرایش شد']);
    }


    /**
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $this->authorize('delete', $page);

        $page->delete();

        return redirect()->route('admin.pages.index')->with(['msg' => 'برگه با موفقیت حذف شد']);
    }

    public function show(Page $page)
    {
        $this->authorize('view', $page);

        return view('admin.pages.show', compact('page'));
    }

    /**
     *
     * @param Request $request
     */
    public function massDestroy()
    {
        Page::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }
}
