<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreRoleRequest;
use App\Http\Requests\Admin\UpdateRoleRequest;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index()
    {
        $this->authorize('view', Role::class);
        $roles = Role::paginate(10);
        return view('admin.roles.index', compact('roles'));
    }

    public function create()
    {
        $this->authorize('create', Role::class);

        $permissions = Permission::all();

        return view('admin.roles.create', compact('permissions'));
    }

    public function show(Role $role)
    {
        $this->authorize('view', $role);

        $role->load('permissions');

        return view('admin.roles.show', compact('role'));
    }

    public function store(StoreRoleRequest $request)
    {
        $this->authorize('create', Role::class);

        $role = Role::create($request->except('permissions'));
        $permissions = $request->input('permissions') ?? [];
        $role->givePermissionTo($permissions);

        return redirect()->route('admin.roles.index')->with(['msg' => 'نقش با موفقیت اضافه شد']);
    }


    public function edit(Role $role)
    {
        $this->authorize('update', $role);

        $permissions = Permission::all();

        return view('admin.roles.edit', compact('role', 'permissions'));
    }

    public function update(Role $role, UpdateRoleRequest $request)
    {
        $this->authorize('update', $role);

        $role->update($request->except('permissions'));
        $permissions = $request->permissions ?? [];
        $role->syncPermissions($permissions);

        return redirect()->route('admin.roles.show', $role)->with(['msg' => 'نقش با موفقیت ویرایش شد']);
    }

    public function destroy(Role $role)
    {
        $this->authorize('delete', $role);

        try {
            $role->delete();
        } catch (\Exception $e) {
        }

        return redirect()->route('admin.roles.index')->with(['msg' => 'نقش با موفقیت حذف شد']);
    }


}
