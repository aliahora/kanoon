<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Slide;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreSlideRequest;
use App\Http\Requests\Admin\UpdateSlideRequest;

class SlideController extends Controller
{
    public function index()
    {
        $this->authorize('view',Slide::class );
        $slides = Slide::all();

        return view('admin.slides.index', compact('slides'));
    }

    public function create()
    {
        $this->authorize('create', Slide::class);
        $articles = Article::latest()->take(10)->get();
        return view('admin.slides.create',compact('articles'));
    }

    public function store(StoreSlideRequest $request)
    {
        $this->authorize('create', Slide::class);

        Slide::create($request->all());

        return redirect()->route('admin.slides.index')->with(['msg' => 'اسلاید با موفقیت اضافه شد']);
    }

    public function edit(Slide $slide)
    {
        $this->authorize('update', $slide);

        $articles = Article::latest()->take(10)->get();

        return view('admin.slides.edit', compact('slide','articles'));
    }

    public function update(UpdateSlideRequest $request, Slide $slide)
    {
        $this->authorize('update', $slide);

        $slide->update($request->all());

        return redirect()->route('admin.slides.index')->with(['msg' => 'اسلاید با موفقیت ویرایش شد']);
    }

    public function destroy(Slide $slide)
    {
        $this->authorize('delete', $slide);

        $slide->delete();

        return redirect()->route('admin.slides.index')->with(['msg' => 'اسلاید با موفقیت حذف شد']);
    }

    public function massDestroy()
    {
        Slide::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }
}
