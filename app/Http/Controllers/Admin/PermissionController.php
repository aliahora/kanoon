<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StorePermissionsRequest;
use App\Http\Requests\Admin\UpdatePermissionsRequest;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function index()
    {
        $this->authorize('view' ,Permission::class);
        $permissions = Permission::paginate(10);

        return view('admin.permissions.index', compact('permissions'));
    }

    public function create()
    {
        $this->authorize('create', Permission::class);
        return view('admin.permissions.create');
    }

    public function store(StorePermissionsRequest $request)
    {
        $this->authorize('create', Permission::class);
        Permission::create($request->all());

        return redirect()->route('admin.permissions.index')->with(['msg' => 'دسترسی با موفقیت اضافه شد']);
    }

    public function edit(Permission $permission)
    {
        $this->authorize('update', $permission);

        return view('admin.permissions.edit', compact('permission'));
    }

    public function update(UpdatePermissionsRequest $request, Permission $permission)
    {
        $this->authorize('update', $permission);

        $permission->update($request->all());

        return redirect()->route('admin.permissions.index')->with(['msg' => 'دسترسی با موفقیت ویرایش شد']);
    }

    public function destroy(Permission $permission)
    {
        $this->authorize('delete', $permission);

        $permission->delete();

        return redirect()->route('admin.permissions.index')->with(['msg' => 'دسترسی با موفقیت حذف شد']);
    }

    public function massDestroy()
    {
        Permission::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }
}
