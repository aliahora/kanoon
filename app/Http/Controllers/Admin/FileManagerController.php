<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class FileManagerController extends Controller
{
    public function index()
    {
        if (!Gate::allows('file_manager')){
            return abort(403);
        }
        return view('admin.file-manager');
    }
}
