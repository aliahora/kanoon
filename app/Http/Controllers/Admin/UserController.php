<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUserRequest;
use App\Http\Requests\Admin\UpdateUserRequest;
use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function index()
    {
        $this->authorize('view', User::class);
        $users = User::paginate(10);
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        $this->authorize('create', User::class);
        $roles = Role::all();

        return view('admin.users.create', compact('roles'));
    }

    public function edit(User $user)
    {
        $this->authorize('update', $user);

        $roles = Role::all();
        return view('admin.users.edit', compact('user', 'roles'));
    }

    public function store(StoreUserRequest $request)
    {
        $this->authorize('create', User::class);

        $request['password'] = bcrypt($request['password']);

        $user = User::create($request->all());
        $role = $request->role ? $request->role : '';
        $user->assignRole($role);

        return redirect()->route('admin.users.index')->with(['msg' => 'کاربر با موفقیت اضافه شد']);
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('update', $user);

        $data = $request->except('password');

        if (!empty($request->password)) {
            $data['password'] = bcrypt($request->password);
        }

        $user->update($data);
        $role = $request->role ? $request->role : '';
        $user->syncRoles($role);
        return redirect()->route('admin.users.show', $user->id)->with(['msg' => 'کاربر با موفقیت ویرایش شد']);
    }


    public function show(User $user)
    {
        $this->authorize('view', $user);

        $user->load('roles');

        return view('admin.users.show', compact('user'));
    }

    public function destroy(User $user)
    {
        $this->authorize('delete', $user);

        try {
            $user->delete();
        } catch (\Exception $e) {
        }

        return redirect()->route('admin.users.index')->with(['msg' => 'کاربر با موفقیت حذف شد']);
    }

    public function massDestroy(Request $request)
    {
        User::whereIn('id', request('ids'))->delete();
        return response()->noContent();
    }
}
