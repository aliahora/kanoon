<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UpdateCommentRequest;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index()
    {
        $this->authorize('view', Comment::class);
        $comments = Comment::latest()->paginate(10);

        return view('admin.comments.index', compact('comments'));
    }

    /**
     * Show the form for editing Comment.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(Comment $comment)
    {
        $this->authorize('update', $comment);

        $comment->readed = true;
        $comment->save();

        return view('admin.comments.edit', compact('comment'));
    }

    /**
     * Update Comment in storage.
     *
     * @param  \App\Http\Requests\Admin\UpdateCommentRequest $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCommentRequest $request, Comment $comment)
    {
        $this->authorize('update', $comment);

        $data = $request->all();

        if (!$request->has('matn')) {
            $data['published'] = !$comment->published;
        }

        $comment->update($data);

        return redirect()->route('admin.comments.index')->with(['msg' => 'نظر با موفقیت ویرایش شد']);
    }


    /**
     * Remove Comment from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Comment $comment)
    {
        $this->authorize('delete', $comment);

        $comment->delete();

        return redirect()->route('admin.comments.index')->with(['msg' => 'نظر با موفقیت حذف شد']);
    }

    public function show(Comment $comment)
    {
        $this->authorize('view', $comment);

        $comment->readed = true;
        $comment->save();

        return view('admin.comments.show', compact('comment'));
    }

    /**
     * Delete all selected Comment at once.
     *
     * @param Request $request
     */
    public function massDestroy()
    {
        Comment::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }
}
