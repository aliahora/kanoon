<?php

namespace App\Http\Controllers\Admin;

use App\Tool;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreToolRequest;
use App\Http\Requests\Admin\UpdateToolRequest;

class ToolController extends Controller
{
    public function index()
    {
        $this->authorize('view', Tool::class);
        $tools = Tool::all();

        return view('admin.tools.index', compact('tools'));
    }

    public function create()
    {
        $this->authorize('create', Tool::class);
        return view('admin.tools.create');
    }

    public function store(StoreToolRequest $request)
    {
        $this->authorize('create', Tool::class);

        if (Tool::count() == 11) {
            return redirect()->route('admin.tools.index')->with(['err' => 'تعداد منوهای کارتی به حداکثر تعداد رسیده اند']);
        }

        Tool::create($request->all());

        return redirect()->route('admin.tools.index')->with(['msg' => 'منوی کارتی با موفقیت اضافه شد']);
    }

    public function edit(Tool $tool)
    {
        $this->authorize('update', $tool);

        return view('admin.tools.edit', compact('tool'));
    }

    public function update(UpdateToolRequest $request, Tool $tool)
    {
        $this->authorize('update', $tool);

        $tool->update($request->all());

        return redirect()->route('admin.tools.index')->with(['msg' => 'اسلاید با موفقیت ویرایش شد']);
    }

    public function destroy(Tool $tool)
    {
        $this->authorize('delete', $tool);

        $tool->delete();

        return redirect()->route('admin.tools.index')->with(['msg' => 'اسلاید با موفقیت حذف شد']);
    }

    public function show(Tool $tool)
    {
        $this->authorize('view', $tool);

        return view('admin.tools.show', compact('tool'));
    }

    public function massDestroy()
    {
        Tool::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }
}
