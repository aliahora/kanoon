<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreArticleRequest;
use App\Http\Requests\Admin\UpdateArticleRequest;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        $this->authorize('view',Article::class );
        $articles = Article::latest()->paginate(10);

        return view('admin.articles.index', compact('articles'));
    }

    public function search(Request $request)
    {
        $articles = Article::where('title','LIKE',"%$request->q%")->latest()->paginate(10);
        $articles->appends(['q' => $request->q]);
        return view('admin.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating new Article.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', Article::class);

        $categories = Category::all();

        return view('admin.articles.create', compact('categories'));
    }

    /**
     * Store a newly created Article in storage.
     *
     * @param  \App\Http\Requests\Admin\StoreArticleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreArticleRequest $request)
    {
        $this->authorize('create', Article::class);

        $data = $request->all();
        $data['user_id'] = auth()->id();
        $article = Article::create($data);
        $article->attachTags(explode(',', $request->tags));

        return redirect()->route('admin.articles.index')->with(['msg' => 'خبر با موفقیت اضافه شد']);
    }


    /**
     * Show the form for editing Article.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(Article $article)
    {
        $this->authorize('update', $article);

        $categories = Category::all();

        return view('admin.articles.edit', compact('article', 'categories'));
    }

    /**
     * Update Article in storage.
     *
     * @param  \App\Http\Requests\Admin\UpdateArticleRequest $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateArticleRequest $request, Article $article)
    {
        $this->authorize('update', $article);

        $data = $request->all();
        $data['user_id'] = auth()->id();

        $article->syncTags(explode(',', $request->tags));
        $article->update($data);

        return redirect()->route('admin.articles.index')->with(['msg' => 'خبر با موفقیت ویرایش شد']);
    }


    /**
     * Remove Article from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Article $article)
    {
        $this->authorize('delete', $article);

        $article->delete();

        return redirect()->route('admin.articles.index')->with(['msg' => 'خبر با موفقیت حذف شد']);
    }

    public function show(Article $article)
    {
        $this->authorize('view', $article);

        return view('admin.articles.show', compact('article'));
    }

    /**
     * Delete all selected Article at once.
     *
     * @param Request $request
     */
    public function massDestroy()
    {
        Article::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }
}
