<?php

namespace App\Http\Controllers\Admin;

use App\Complaint;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ComplaintController extends Controller
{
    public function index()
    {
        $this->authorize('view', Complaint::class);
        $complaints = Complaint::latest()->paginate(10);

        return view('admin.complaints.index', compact('complaints'));
    }

    public function search(Request $request)
    {
        $complaints = Complaint::where('code','LIKE',"%$request->q%")->latest()->paginate(10);
        $complaints->appends(['q' => $request->q]);
        return view('admin.complaints.index', compact('complaints'));
    }

    /**
     * Update Complaint in storage.
     *
     * @param  \App\Http\Requests\Admin\UpdateComplaintRequest $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Complaint $complaint)
    {
        $this->authorize('update', $complaint);

        $complaint->status = $request->status;

        $complaint->save();

        return redirect()->route('admin.complaints.index')->with(['msg' => 'شکایت با موفقیت ویرایش شد']);
    }


    /**
     * Remove Complaint from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Complaint $complaint)
    {
        $this->authorize('delete', $complaint);

        $complaint->delete();

        return redirect()->route('admin.complaints.index')->with(['msg' => 'شکایت با موفقیت حذف شد']);
    }

    public function show(Complaint $complaint)
    {
        $this->authorize('view', $complaint);

        $complaint->readed = true;
        $complaint->save();

        return view('admin.complaints.show', compact('complaint'));
    }

    /**
     * Delete all selected Complaint at once.
     *
     * @param Request $request
     */
    public function massDestroy()
    {
        Complaint::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }
}
