<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Tool
 *
 * @property int $id
 * @property string $title
 * @property string $url
 * @property int $order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Tool newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tool newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tool query()
 * @method static \Illuminate\Database\Eloquent\Builder|Tool whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tool whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tool whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tool whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tool whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tool whereUrl($value)
 * @mixin \Eloquent
 */
class Tool extends Model
{
    protected $fillable = [
        'title',
        'url',
        'order',

    ];
}
