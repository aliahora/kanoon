<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Slide
 *
 * @property int $id
 * @property int $article_id
 * @method static \Illuminate\Database\Eloquent\Builder|Slide newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Slide newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Slide query()
 * @method static \Illuminate\Database\Eloquent\Builder|Slide whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slide whereId($value)
 * @mixin \Eloquent
 * @property string|null $image
 * @property string|null $caption
 * @property string|null $link
 * @property int $order
 * @property-read \App\Article|null $article
 * @method static \Illuminate\Database\Eloquent\Builder|Slide whereCaption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slide whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slide whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Slide whereOrder($value)
 */
class Slide extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'caption',
        'image',
        'link',
        'order',
        'article_id'
    ];

    public function article()
    {
        return $this->belongsTo('App\Article');
    }
}
