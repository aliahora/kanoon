<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Comment
 *
 * @property-read \App\Article $article
 * @method static \Illuminate\Database\Eloquent\Builder|Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $article_id
 * @property int|null $parent_id
 * @property string $name
 * @property string|null $email
 * @property string $body
 * @property int $published
 * @property int $readed
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Comment[] $replys
 * @property-read int|null $replys_count
 * @method static \Illuminate\Database\Eloquent\Builder|Comment notReaded()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereReaded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereUpdatedAt($value)
 */
class Comment extends Model
{
    protected $fillable = [
        'parent_id',
        'article_id',
        'name',
        'email',
        'matn',
        'published',
        'readed',
    ];

    public function article()
    {
        return $this->belongsTo('App\Article');
    }

    public function scopeNotReaded($query)
    {
        return $query->whereReaded(false);
    }

    public function replies()
    {
        return $this->hasMany('App\Comment','parent_id');
    }
}
