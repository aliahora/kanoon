<?php

namespace App\Policies;

use Spatie\Permission\Models\Permission;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {

    }

    public function view(User $user)
    {
        if ($user === null) {
            return false;
        }

        if ($user->can('permissions_view')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user === null) {
            return false;
        }

        if ($user->can('permissions_store')) {
            return true;
        }

        return false;
    }

    public function update(User $user)
    {
        if ($user === null) {
            return false;
        }

        // admin overrides published status
        if ($user->can('permissions_update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user)
    {
        if ($user === null) {
            return false;
        }

        // admin overrides published status
        if ($user->can('permissions_delete')) {
            return true;
        }

        return false;
    }

    public function restore(User $user)
    {

    }

    public function forceDelete(User $user)
    {

    }
}
