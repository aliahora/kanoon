<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ToolPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {

    }

    public function view(User $user)
    {
        if ($user === null) {
        return false;
    }

        if ($user->can('tools_view')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user === null) {
            return false;
        }

        if ($user->can('tools_store')) {
            return true;
        }

        return false;
    }

    public function update(User $user)
    {
        if ($user === null) {
            return false;
        }

        if ($user->can('tools_update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user)
    {
        if ($user === null) {
            return false;
        }

        if ($user->can('tools_delete')) {
            return true;
        }

        return false;
    }

    public function restore(User $user)
    {

    }

    public function forceDelete(User $user)
    {

    }
}
