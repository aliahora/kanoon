<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {

    }

    public function view(User $user)
    {
        if ($user === null) {
        return false;
    }

        if ($user->can('comments_view')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user === null) {
            return false;
        }

        if ($user->can('comments_store')) {
            return true;
        }

        return false;
    }

    public function update(User $user)
    {
        if ($user === null) {
            return false;
        }

        if ($user->can('comments_update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user)
    {
        if ($user === null) {
            return false;
        }

        if ($user->can('comments_delete')) {
            return true;
        }

        return false;
    }

    public function restore(User $user)
    {

    }

    public function forceDelete(User $user)
    {

    }
}
