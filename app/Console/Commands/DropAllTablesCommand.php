<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DropAllTablesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:drop {--table=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drop all tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $argument = $this->option('table');
        if ($argument) {
            DB::statement("SET foreign_key_checks=0");
            Schema::drop($argument);
            $this->info("Dropping: $argument");

            DB::statement("SET foreign_key_checks=1");

            $this->info("table successfully dropped!");
            return;
        }


        $databaseName = DB::getDatabaseName();
        $tables = DB::select("SELECT * FROM information_schema.tables WHERE table_schema = '$databaseName'");

        if(!$tables)
            return $this->info("No tables found");

        DB::statement("SET foreign_key_checks=0");
        
        foreach ($tables as $table) {
            $name = $table->TABLE_NAME;
            Schema::drop($name);
            $this->info("Dropping: $name");
        }

        DB::statement("SET foreign_key_checks=1");

        $this->info("Drop all tables completed successfully!");
    }
}
