<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TruncateAllTablesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:truncate {--table=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Truncate all tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $argument = $this->option('table');
        if ($argument) {
            DB::statement("SET foreign_key_checks=0");
            DB::table($argument)->truncate();
            $this->info("Truncating: $argument");

            DB::statement("SET foreign_key_checks=1");

            $this->info("table successfully truncated!");
            return;
        }

        $databaseName = DB::getDatabaseName();
        $tables = DB::select("SELECT * FROM information_schema.tables WHERE table_schema = '$databaseName'");



        DB::statement("SET foreign_key_checks=0");

        foreach ($tables as $table) {
            $name = $table->TABLE_NAME;
            DB::table($name)->truncate();
            $this->info("Truncating: $name");
        }

        DB::statement("SET foreign_key_checks=1");

        $this->info("Truncate all tables completed successfully!");
    }
}
