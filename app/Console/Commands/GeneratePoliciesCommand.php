<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class GeneratePoliciesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'policy:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate policies based on model list automatically';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $policies_path = app_path('/Policies');
        $stub_path = app_path('/Console/Commands/stubs/StubPolicy.stub');

        $model_files = collect(File::files(app_path()));

        $except_models = [
            'Image',
            'User',
            'Tag',
            'Attachment'
        ];

        $models = $model_files->filter(function ($file) use ($except_models) {
            $model_name = str_replace('.php', '', pathinfo($file, PATHINFO_BASENAME));
            return !in_array($model_name, $except_models);
        });

        $models = $models->map(function ($file) {
            $model_name = str_replace('.php', '', pathinfo($file, PATHINFO_BASENAME));
            return $model_name;
        });

        if (!File::exists($policies_path)) {
            File::makeDirectory($policies_path);
        }

        foreach ($models as $model) {
            $stub = File::get($stub_path);

            $search = array("%MODEL%", "%MODEL_PLURAL%");
            $replace = array($model, Str::lower(Str::plural($model)));

            $new_stub = str_replace($search, $replace, $stub);

            $created = File::put($policies_path . '/' . $model . 'Policy.php', $new_stub);

            if ($created)
                $this->info($model . 'Policy created successfully');
            else
                $this->error($model . 'Policy not created');
        }

        return 0;
    }
}
