<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Complaint
 *
 * @property int $id
 * @property int $code
 * @property string $shaki_name
 * @property int $kode_meli
 * @property int $shaki_tel
 * @property string $shaki_address
 * @property string $matn
 * @property string $vakil_name
 * @property string $vakil_address
 * @property int $vakil_tel
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Attachment[] $attachments
 * @property-read int|null $attachments_count
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint query()
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereKodeMeli($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereMatn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereShakiAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereShakiName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereShakiTel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereVakilAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereVakilName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereVakilTel($value)
 * @mixin \Eloquent
 * @property string $status
 * @property int $readed
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint notReaded()
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereReaded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereStatus($value)
 */
class Complaint extends Model
{
    const CREATED_STATUS = "created";
    const REFER_TO_PEACE_STATUS = " refer_to_peace";
    const REFER_TO_DADSRA_STATUS = "refer_to_dadsara";
    const REFER_TO_SHOBE1_STATUS = " refer_to_shobe1";
    const REFER_TO_SHOBE2_STATUS = "refer_to_shobe2";
    const REFER_TO_SHOBE3_STATUS = "refer_to_shobe3";
    const REFER_TO_SHOBE4_STATUS = "refer_to_shobe4";
    const REFER_TO_SHOBE5_STATUS = "refer_to_shobe5";
    const REFER_TO_SHOBE6_STATUS = "refer_to_shobe6";
    const EXPORT_STATUS = "export";
    const REFER_TO_ENTEZAMI1_STATUS = "refer_to_entezami1";
    const REFER_TO_ENTEZAMI2_STATUS = "refer_to_entezami2";
    const REFER_TO_ENTEZAMI3_STATUS = "refer_to_entezami3";
    const JEDGE_COMPLETE_STATUS_STATUS = "judge_complete";


    const STATUSES_ARRAY = [
        'ثبت شده' => Complaint::CREATED_STATUS,
        'ارجاع به کمیسیون صلح و سازش' => Complaint::REFER_TO_PEACE_STATUS,
        'ارجاع به دادسرا' => Complaint::REFER_TO_DADSRA_STATUS,
        'ارجاع به شعبه1 دادسرای انتظامی' => Complaint::REFER_TO_SHOBE1_STATUS,
        'ارجاع به شعبه2 دادسرای انتظامی' => Complaint::REFER_TO_SHOBE2_STATUS,
        'ارجاع به شعبه3 دادسرای انتظامی' => Complaint::REFER_TO_SHOBE3_STATUS,
        'ارجاع به شعبه4 دادسرای انتظامی' => Complaint::REFER_TO_SHOBE4_STATUS,
        'ارجاع به شعبه5 دادسرای انتظامی' => Complaint::REFER_TO_SHOBE5_STATUS,
        'ارجاع به شعبه6 دادسرای انتظامی' => Complaint::REFER_TO_SHOBE6_STATUS,
        'قرار صادر شده است' => Complaint::EXPORT_STATUS,
        'ارجاع به شعبه1 دادگاه انتظامی' => Complaint::REFER_TO_ENTEZAMI1_STATUS,
        'ارجاع به شعبه2 دادگاه انتظامی' => Complaint::REFER_TO_ENTEZAMI2_STATUS,
        'ارجاع به شعبه3 دادگاه انتظامی' => Complaint::REFER_TO_ENTEZAMI3_STATUS,
        'رای صادر شده است' => Complaint::JEDGE_COMPLETE_STATUS_STATUS,
    ];


    protected $fillable = [
        'shaki_name',
        'kode_meli',
        'shaki_address',
        'vakil_name',
        'vakil_address',
        'shaki_tel',
        'vakil_tel',
        'matn',
        'status',
        'readed',
        'code'
    ];

    public function attachments()
    {
        return $this->hasMany('App\Attachment');
    }

    public function scopeNotReaded($query)
    {
        return $query->whereReaded(false);
    }

    public static function translateStatus($status): string
    {
        $translated = '';

        switch ($status) {
            case self::CREATED_STATUS:
                $translated = 'ثبت شده';
                break;
            case self::PENDING_STATUS:
                $translated = 'در حال پیگیری';
                break;
            case self::DENIED_STATUS:
                $translated = 'رد شده';
                break;
            case self::ACCEPTED_STATUS:
                $translated = 'پذیرفته شده';
                break;
            case self::PROBLEM_STATUS:
                $translated = 'اطلاعات ناقص';
                break;
            case self::FINISHED_STATUS:
                $translated = 'خاتمه یافته';
                break;
        }
        return $translated;
    }
}
