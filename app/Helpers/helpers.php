<?php

use App\Helpers\Thumb;

if (!function_exists('thumb')) {
    function thumb($url): string
    {
        return Thumb::get($url);
    }
    function generate_complaint_code(): int
    {
        return rand(1000000000, 9999999999);
    }
}
