<?php

namespace App\Helpers;

class Thumb
{
    public static function get($url)
    {
        $filename = pathinfo($url, PATHINFO_BASENAME);
        return explode($filename,$url)[0] . 'thumbs/' . $filename;
    }
}
