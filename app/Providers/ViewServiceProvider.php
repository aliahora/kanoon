<?php

namespace App\Providers;

use App\Category;
use App\Comment;
use App\Complaint;
use App\Menu;
use App\Message;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Shetabit\Visitor\Models\Visit;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('site/*', function ($view) {
            $view->with([
                'statistics' => $this->getStatistics(),
                'menus' => Menu::orderBy('order', 'asc')->get()->toTree(),
                'categories' => Category::all(),
            ]);
        });

        View::composer('admin/*', function ($view) {
            $view->with([
                'countOfNewMessages' => $this->getCountOfNewMessages(),
                'countOfNewComments' => $this->getCountOfNewComments(),
                'countOfNewComplaints' => $this->getCountOfNewComplaints(),
            ]);
        });
    }

    public function getCountOfNewMessages()
    {
        return Message::notReaded()->count();
    }

    public function getCountOfNewComments()
    {
        return Comment::notReaded()->count();
    }

    public function getCountOfNewComplaints()
    {
        return Complaint::notReaded()->count();
    }

    public function getStatistics(): array
    {
        return [
            'today' => Visit::where('created_at', '>=', Carbon::today()->toDateString()) // 00:00
                ->count(),
            'yesterday' => Visit::where('created_at', '>=', Carbon::today()->subDay()->toDateString()) // 20:55
                ->where('created_at', '<=', Carbon::today()->toDateString())
                ->count(),
            'this_week' => Visit::where('created_at', '>=', (new Carbon())->startOfWeek(Carbon::SATURDAY)->toDateString()) // 20:55
                ->count(),
            'last_week' => Visit::where('created_at', '>=', (new Carbon())->subWeek()->startOfWeek(Carbon::SATURDAY)->toDateString()) // 20:55
                ->where('created_at', '<=', (new Carbon())->startOfWeek(Carbon::SATURDAY)->toDateString())
                ->count(),
            'this_month' => Visit::where('created_at', '>=', verta()->startMonth()->datetime()) // 20:55
                ->count(),
            'last_month' => Visit::where('created_at', '>=', verta()->subMonth()->startMonth()->datetime()) // 20:55
                ->where('created_at', '<=', verta()->startMonth()->datetime())
                ->count(),
            'total' => Visit::count()
        ];
    }
}
